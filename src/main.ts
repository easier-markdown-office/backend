import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { DatabaseService } from './v1/database/database.service';
import { SetUserAgentMiddleware } from './v1/middlewares/set-user-agent.middleware';

process.IS_DEVELOPMENT = !(process.env.NODE_ENV === 'production');

const PORT = process.env.PORT || 3737;
const ORIGIN_URL = process.IS_DEVELOPMENT ? '*' : 'https://prev.lui-studio.net';

new DatabaseService().connectDatabase();

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    credentials: true,
    origin: ORIGIN_URL,
  });
  app.use(SetUserAgentMiddleware.use);
  // app.useWebSocketAdapter(new AuthWsIoAdapter(app));

  /* Swagger */
  const options = new DocumentBuilder()
    .setTitle('API of "Easier Markdown Office"')
    .setDescription(
      'The API provides endpoints for <i>Easier Markdown Office<i>.',
    )
    .setContact('Team', '', 'contact@lui-studio.net')
    .addBearerAuth();

  options
    .addTag('v1', '')
    .addTag(
      'user',
      'Responsible for handling all actions and requests concerning the users.',
    );

  const doc = options.build();
  const document = SwaggerModule.createDocument(app, doc);
  SwaggerModule.setup('/api/v1/', app, document, {
    customCss: '.swagger-ui .topbar { display: none }',
  });

  await app.listen(PORT);
}
bootstrap();
