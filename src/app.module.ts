import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './v1/user/user.module';
import { DocumentModule } from './v1/document/document.module';
import { CursorModule } from './v1/cursor/cursor.module';
import { AdminModule } from './v1/admin/admin.module';

@Module({
  imports: [UserModule, DocumentModule, CursorModule, AdminModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
