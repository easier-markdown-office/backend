import { Server, Socket } from 'socket.io';
import { Types } from 'mongoose';

const MAGIC_MONGO_OBJECT_ID_LENGTH = 24;

export default class UtilityService {
  // implemented in socket.io 3.x....
  public static isInRoom(client: Socket, roomName: string): boolean {
    for (const room of Object.keys(client.rooms)) {
      if (client.rooms[room] === roomName) {
        return true;
      }
    }

    return false;
  }

  public static getMongoObjectIDFromRoom(
    client: Socket,
    prefix: string,
  ): string | null {
    for (const room of Object.keys(client.rooms)) {
      if (!room.startsWith(prefix)) continue;

      const withoutPrefix = room.substring(prefix.length);
      if (withoutPrefix.length !== MAGIC_MONGO_OBJECT_ID_LENGTH) continue;

      try {
        return new Types.ObjectId(withoutPrefix).toHexString();
      } catch (e) {
        console.log(e);
        //
      }
    }

    return null;
  }

  public static getSocketsFromRoomName(
    server: Server,
    roomName: string,
  ): Socket[] {
    const result: Socket[] = [];

    for (const socketID of Object.keys(
      server.sockets.adapter.rooms[roomName]?.sockets ?? {},
    )) {
      const client = server.sockets.sockets[socketID];
      if (client) result.push(client);
    }

    return result;
  }
}
