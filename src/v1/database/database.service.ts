import { hostname } from 'os';

import { Injectable } from '@nestjs/common';
import { connect, connection, Error as MError } from 'mongoose';

import { database, collectionNames } from '../../../configs/config.json';

/**
 * This class (the DatabaseService) is the provider for the primary interaction of the server with the
 * database: The connection to the database. It also handles various other issues related to the connection.
 */
@Injectable()
export class DatabaseService {
  /** Specifies whether it is the first connection between the server and the database.  */
  private _firstConnect = true;

  private _rejoinCounter = 0;

  /**
   * Constructor for the DatabaseService which sets all the events for the database connection.
   */
  public constructor() {
    this.setConnectionEvents();
  }

  /**
   * Handles the connection event "connected".
   */
  /* istanbul ignore next */
  private static listenerConnected(): void {
    console.info('Connection established successfully, no errors encountered.');
  }

  /**
   * This function generates all required collections.
   * This function is NOT required for the functionality, because Mongoose generates the collection itself.
   * This function should help with the development as long as all the collections are not available.
   */
  private static async checkCollection(): Promise<void> {
    // iterates over the collection names. get the values of dic, adds an "s" and transform collection name to lower case
    for (const collectionName of Object.values(collectionNames).map(
      (val) => `${val.toLocaleLowerCase()}s`,
    )) {
      try {
        connection.db
          .listCollections({
            name: collectionName,
          })
          .next((error, result) => {
            if (!result) connection.db.createCollection(collectionName);

            if (error)
              console.error(
                `Collection creation failed. ${collectionName} exists already?`,
                error.message,
              );
          });
      } catch (e) {
        console.error(
          `Collection creation failed. ${collectionName} exists already?`,
          e.message,
        );
      }
    }
  }

  /**
   * This function creates the connection uri to a database.
   * @param protocol The protocol of the connection (default: mongodb)
   * @param uri The domain part.
   * @param port The target port. This argument is optional.
   * @param args The optional arguments for the connection.
   */
  private static getURL(
    protocol: string,
    uri: string,
    port?: number,
    args?: string,
  ): string {
    const portPart = port ? `:${port}` : '';
    const argsPart = args ? `?${args}` : '';
    return `${protocol}://${uri}${portPart}/${database.name}${argsPart}`;
  }

  /**
   * Function which aims to connect the server to the database.
   */
  public async connectDatabase(): Promise<void> {
    const firstConnect = this._firstConnect;
    this._firstConnect = false;

    // check whether server is in development mode and set the database uri to database's development URI or production URI
    let url: string;
    if (!process.IS_DEVELOPMENT)
      url = DatabaseService.getURL(
        database.production.protocol,
        database.production.uri,
        database.production.port,
        database.production.args,
      );
    else
      url = DatabaseService.getURL(
        database.development.protocol,
        process.platform === 'win32' ? hostname() : 'localhost',
        database.development.port,
        database.development.args,
      );

    try {
      await connect(url, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        // connectTimeoutMS: TIMEOUT_MS,
        // socketTimeoutMS: TIMEOUT_MS,
        serverSelectionTimeoutMS: database.serverSelectionTimeoutMS,
      });

      /*
       In the case of the first connect of the server to the db, the db will be empty, so the goal is
       to set up all the necessary data in the database for the server to function properly.
       */
      if (firstConnect) {
        await DatabaseService.checkCollection();
      }
      this._rejoinCounter = 0;
    } catch (e) {
      if (e instanceof MError) {
        if (!this.rejoin() && this._rejoinCounter >= database.rejoinTrials)
          console.log('The database is not accessible.');

        return;
      }
      console.error(e);
    }
  }

  /**
   * Function to close the connection to the database.
   */
  public async disconnectDatabase(): Promise<void> {
    await connection.close();
  }

  /**
   * Sets all helpful events for a connection, including a listener in the event the connection
   * has been successful, a bind in the event that the connection has been terminated and an error if
   * there has been an error while attempting to connect.
   */
  private setConnectionEvents(): void {
    connection.removeAllListeners();
    connection.once('connected', DatabaseService.listenerConnected);
    connection.once('disconnected', this.listenerDisconnected.bind(this));
    connection.once('error', this.listenerOnError.bind(this));
  }

  /**
   * Handles the connection event "disconnected".
   */
  /* istanbul ignore next */
  private listenerDisconnected(): void {
    console.info('Connection to database terminated, attempting to reconnect.');
    this.connectDatabase();
  }

  /**
   * Handles the connection event "error".
   */
  /* istanbul ignore next */
  private listenerOnError(): void {
    console.info(
      'Red Alert. Encountered an error while attempting to connect.',
    );

    if (connection.readyState === 0) this.rejoin();
  }

  private rejoin(): boolean {
    if (this._rejoinCounter < database.rejoinTrials) {
      // istanbul ignore next
      if (connection.readyState === 0) {
        console.info('Rejoin in few seconds...');
        setTimeout(this.connectDatabase.bind(this), database.rejoinDelayMS);
        ++this._rejoinCounter;
        return true;
      }
    }

    return false;
  }
}
