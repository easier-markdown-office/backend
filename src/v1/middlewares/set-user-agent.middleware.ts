import { Request } from 'express';

// TODO: documentation
export class SetUserAgentMiddleware {
  public static async use(
    req: Request,
    res: Response,
    next: () => void,
  ): Promise<void> {
    req.userAgent = req.get('user-agent') ?? '';

    // debug to simulate a delay (frontend loading stuff)
    // await new Promise(resolve => setTimeout(resolve, 1000 * 1.1));

    // debug to simulate a delay (frontend loading stuff)
    // await new Promise((resolve) => setTimeout(resolve, 1000 * 10.1));

    next();
  }
}
