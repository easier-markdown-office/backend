import {
  CanActivate,
  ExecutionContext,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Request } from 'express';
import UserDBM from '../models/schemas/user.schema';

@Injectable()
export class EmailExistsGuard implements CanActivate {
  public static async checkAndSet(
    client: UserInformation,
    emailAddress: string,
  ): Promise<boolean> {
    try {
      const user = await UserDBM.findOne({
        email: emailAddress,
      } as any);

      if (user) {
        client.checkedEmailAddressUser = user;
        return true;
      }
    } catch (e) {
      // ignore exceptions
    }

    throw new NotFoundException();
  }

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest<Request>();
    return EmailExistsGuard.checkAndSet(req, req.body['email'] ?? '');
  }
}
