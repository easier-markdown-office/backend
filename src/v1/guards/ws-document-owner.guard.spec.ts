import { WsDocumentOwnerGuard } from './ws-document-owner.guard';

describe('WsDocumentOwnerGuard', () => {
  it('should be defined', () => {
    expect(new WsDocumentOwnerGuard()).toBeDefined();
  });
});
