import {
  BadRequestException,
  CanActivate,
  ExecutionContext,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Request } from 'express';
import { Types } from 'mongoose';
import DocumentDBM from '../models/schemas/document.schema';

@Injectable()
export class DocumentContributorGuard implements CanActivate {
  public static async checkAndSet(
    client: UserInformation,
    documentID: string,
  ): Promise<boolean> {
    try {
      const castedDocID = new Types.ObjectId(documentID);
      const doc = await DocumentDBM.findOne({
        _id: castedDocID,
        contributors: client.user,
      } as any);

      if (!doc) throw new NotFoundException();

      await doc.populate('contributors').execPopulate();
      client.selectedDocument = doc;
      return true;
    } catch (e) {
      if (e instanceof NotFoundException) throw e;

      throw new BadRequestException();
    }
  }

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest<Request>();
    return DocumentContributorGuard.checkAndSet(req, req.params['docID'] ?? '');
  }
}
