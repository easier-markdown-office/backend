import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Socket } from 'socket.io';
import { EErrorCode } from '../submodule-models/error-code.enum';
import { AuthGuard } from './auth.guard';
import UserDocument from '../models/documents/user.document';

@Injectable()
export class WsAuthGuard implements CanActivate {
  public static async authUserFromSocketClient(
    client: Socket,
  ): Promise<boolean> {
    let user: UserDocument | null = null;
    try {
      user = await AuthGuard.authUser(
        AuthGuard.extractTokenFromBearerHeader(client.handshake?.query?.token),
        client.handshake.headers['user-agent'] ?? '',
      );
    } catch (e) {}

    if (!user) {
      console.debug('failed');
      client.error(EErrorCode.authFailed);
      client.disconnect(true);
      return false;
    }

    client.user = user ?? undefined;
    return !!user;
  }

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const client = context.switchToWs().getClient<Socket>();
    client.token = client.handshake.headers['user-agent'] ?? '';

    return WsAuthGuard.authUserFromSocketClient(client);
  }
}
