import { DocumentOwnerGuard } from './document-owner.guard';

describe('DocumentOwnerGuard', () => {
  it('should be defined', () => {
    expect(new DocumentOwnerGuard()).toBeDefined();
  });
});
