import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Socket } from 'socket.io';
import UtilityService from '../utility.service';
import { ERoom } from '../models/room.enum';
import UserDocument from '../models/documents/user.document';
import { DocumentContributorGuard } from './document-contributor.guard';

@Injectable()
export class WsDocumentContributorGuard implements CanActivate {
  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const client = context.switchToWs().getClient<Socket>();
    const userID = UtilityService.getMongoObjectIDFromRoom(client, ERoom.user);
    if (!userID) return false;
    client.user = { id: userID, _id: userID } as UserDocument; // fake DBM

    const data = context.switchToWs().getData();
    return DocumentContributorGuard.checkAndSet(client, data['id'] ?? data);
  }
}
