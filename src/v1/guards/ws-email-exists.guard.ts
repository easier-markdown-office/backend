import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { EmailExistsGuard } from './email-exists.guard';
import { Socket } from 'socket.io';

@Injectable()
export class WsEmailExistsGuard implements CanActivate {
  public canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const client = context.switchToWs().getClient<Socket>();
    const data = context.switchToWs().getData();
    return EmailExistsGuard.checkAndSet(client, data.email ?? '');
  }
}
