import { WsAuthGuard } from './ws-auth.guard';
import { Test, TestingModule } from '@nestjs/testing';

describe('WsAuthGuard', () => {
  let service: WsAuthGuard;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WsAuthGuard],
    }).compile();

    service = module.get<WsAuthGuard>(WsAuthGuard);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
