import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Socket } from 'socket.io';
import UtilityService from '../utility.service';
import { ERoom } from '../models/room.enum';
import { DocumentOwnerGuard } from './document-owner.guard';
import UserDBM from '../models/schemas/user.schema';

@Injectable()
export class WsDocumentOwnerGuard implements CanActivate {
  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const client = context.switchToWs().getClient<Socket>();
    const userID = UtilityService.getMongoObjectIDFromRoom(client, ERoom.user);
    if (!userID) return false;
    client.user = (await UserDBM.findOne({ _id: userID })) ?? undefined;
    if (!client.user) return false;

    const data = context.switchToWs().getData();
    return DocumentOwnerGuard.checkAndSet(client, data['id'] ?? data);
  }
}
