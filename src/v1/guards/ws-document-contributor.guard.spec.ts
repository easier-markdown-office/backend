import { WsDocumentContributorGuard } from './ws-document-contributor.guard';

describe('WsDocumentContributorGuard', () => {
  it('should be defined', () => {
    expect(new WsDocumentContributorGuard()).toBeDefined();
  });
});
