import { DocumentContributorGuard } from './document-contributor.guard';

describe('ContributorGuard', () => {
  it('should be defined', () => {
    expect(new DocumentContributorGuard()).toBeDefined();
  });
});
