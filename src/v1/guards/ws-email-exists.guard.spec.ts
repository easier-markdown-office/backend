import { WsEmailExistsGuard } from './ws-email-exists.guard';

describe('WsEmailExistsGuard', () => {
  it('should be defined', () => {
    expect(new WsEmailExistsGuard()).toBeDefined();
  });
});
