import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from './auth.guard';
import { Request } from 'express';

const ADMIN_PASSWORD = 'Luigi600';

@Injectable()
export class AdminAuthGuard implements CanActivate {
  public canActivate(context: ExecutionContext): boolean {
    const token = AuthGuard.extractTokenFromBearerHeader(
      context.switchToHttp().getRequest<Request>().header('Authorization') ??
        '',
    );
    return token === ADMIN_PASSWORD;
  }
}
