import {
  Body,
  Controller,
  Get,
  Post,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { ApiBearerAuth, ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';
import LoginResultDTO from './models/login-result.dto';
import LoginDataDTO from './models/login-data.dto';
import { GlobalUserService } from '../global-user/global-user.service';
import UserDTO from './models/user.dto';
import UserDocument from '../models/documents/user.document';
import { AuthGuard } from '../guards/auth.guard';
import ValidationPipe from '../pipes/validation.pipe';

@Controller('user')
@ApiTags('user')
export class UserController {
  public constructor(private readonly service: GlobalUserService) {}

  @Post('login')
  @ApiResponse({
    type: LoginResultDTO,
    description: '...',
  })
  @ApiBody({
    type: LoginDataDTO,
  })
  public async login(
    @Req() req: Request,
    @Body(new ValidationPipe<LoginDataDTO>(LoginDataDTO)) user: LoginDataDTO,
  ): Promise<LoginResultDTO> {
    return await this.service.login(user, req.userAgent);
  }

  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @Get('')
  @ApiResponse({
    type: UserDTO,
    description: '...',
  })
  public async user(@Req() req: Request): Promise<UserDTO> {
    return await this.service.getDocuments(req.user as UserDocument); // checked by auth middleware
  }

  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @Put('/logout')
  public async logout(@Req() req: Request): Promise<void> {
    await this.service.logout(
      req.user as UserDocument, // checked by auth middleware
      req.token as string, // checked by auth middleware
    );
  }
}
