import { ApiProperty } from '@nestjs/swagger';
import { IsString, MinLength, ValidateNested } from 'class-validator';
import { default as UserDTOSuper } from '../../submodule-models/user.dto';
import DocumentDTO from '../../models/document.dto';
import { USERNAME_MIN_LENGTH } from '../../submodule-models/constants';

/**
 * Represents the result dto model for a successful login.
 */
export default class UserDTO extends UserDTOSuper {
  /** The token of the user. */
  @ApiProperty({
    description: 'The name of the user.',
  })
  @IsString()
  @MinLength(USERNAME_MIN_LENGTH)
  public username!: string;

  @ApiProperty({
    type: DocumentDTO,
    description: '...',
    isArray: true,
  })
  @ValidateNested()
  public documents!: DocumentDTO[];

  @ApiProperty({
    type: DocumentDTO,
    description: '...',
    isArray: true,
  })
  @ValidateNested()
  public collaboDocs!: DocumentDTO[];

  public constructor(model: Partial<UserDTO>) {
    super(model);
  }
}
