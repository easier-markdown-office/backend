import { ApiProperty } from '@nestjs/swagger';
import { IsString, MinLength } from 'class-validator';

import { default as LoginDataDTOSuper } from '../../submodule-models/login-data.dto';
import {
  PASSWORD_MIN_LENGTH,
  USERNAME_MIN_LENGTH,
} from '../../submodule-models/constants';

/**
 * Represents a simple data object to transfer the username/email address and the password.
 * This class is used to send the data as header content, so that the content is encrypted
 * (when SSL is active).
 */
export default class LoginDataDTO extends LoginDataDTOSuper {
  /** Represents the username OR the email address of the user. */
  @ApiProperty({
    description: 'Represents the username OR the email address of the user.',
  })
  @IsString()
  @MinLength(USERNAME_MIN_LENGTH)
  public user!: string;

  /** Represents the password of the user (will hash in the db). */
  @ApiProperty({
    description: 'Represents the password of the user (will hash in the db).',
  })
  @IsString()
  @MinLength(PASSWORD_MIN_LENGTH)
  public password!: string;

  /**
   * This is the constructor of the DTO. Here we set the given parameters as the attribute
   * values of the DTO.
   * @param model This model already represents an object that contains the same information as the actual DTO.
   */
  public constructor(model: Partial<LoginDataDTO>) {
    super(model);
  }
}
