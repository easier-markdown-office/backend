import { Module } from '@nestjs/common';
import { UserGateway } from './user.gateway';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { GlobalUserModule } from '../global-user/global-user.module';

@Module({
  providers: [UserGateway, UserService],
  controllers: [UserController],
  imports: [GlobalUserModule],
})
export class UserModule {}
