import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { EventArgs } from '../document/models/event-args.model';
import { UserService } from './user.service';
import { USER_EVENTS } from '../submodule-models/gateway-addresses.constants';
import { GlobalUserService } from '../global-user/global-user.service';
import UtilityService from '../utility.service';
import { WsAuthGuard } from '../guards/ws-auth.guard';
import UserDocument from '../models/documents/user.document';

@WebSocketGateway()
export class UserGateway
  implements OnGatewayConnection<Socket>, OnGatewayDisconnect<Socket> {
  @WebSocketServer()
  public server: Server | undefined;

  public constructor(
    private readonly service: UserService,
    private readonly globalUserService: GlobalUserService,
  ) {
    this.globalUserService.roomChanged.subscribe(this.roomChanged.bind(this));
  }

  public async handleConnection(client: Socket): Promise<void> {
    if (!(await WsAuthGuard.authUserFromSocketClient(client))) return;

    console.debug('login successful');
    for (const room of await this.globalUserService.getRooms(
      client.user as UserDocument, // checked by the middleware
    )) {
      client.join(room);
    }

    this.globalUserService.newConnection.next(client);
  }

  public handleDisconnect(client: Socket): void {
    // remark: The client room list is already empty to this moment...

    const room = this.service.getAndRemoveLastRoomFromUser(client.id);
    if (room) {
      const clientsCount = Object.keys(
        this.server?.sockets.adapter.rooms[room]?.sockets ?? {},
      ).length; // -1 is not necessary because the user is already "removed" from the room

      this.server?.in(room).emit(USER_EVENTS.counter, clientsCount);
    }
  }

  private roomChanged(obj: EventArgs): void {
    obj.oldRooms.forEach((r) => {
      this.server?.emit(
        USER_EVENTS.counter,
        UtilityService.getSocketsFromRoomName(this.server, r).length,
      );
    });

    this.service.setLastRoomFromUser(obj.client.id, obj.newRoom);
    this.server
      ?.in(obj.newRoom)
      .emit(
        USER_EVENTS.counter,
        UtilityService.getSocketsFromRoomName(this.server, obj.newRoom).length,
      );
  }
}
