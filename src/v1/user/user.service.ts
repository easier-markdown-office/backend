import { Injectable } from '@nestjs/common';

@Injectable()
export class UserService {
  private readonly _lastRoomFromUser: { [key: string]: string } = {}; // key = client ID, value = room name

  public setLastRoomFromUser(clientID: string, roomName: string): void {
    this._lastRoomFromUser[clientID] = roomName;
  }

  public getLastRoomFromUser(clientID: string): string | null {
    return this._lastRoomFromUser[clientID];
  }

  public getAndRemoveLastRoomFromUser(clientID: string): string | null {
    const lastRoom = this.getLastRoomFromUser(clientID);
    delete this._lastRoomFromUser[clientID];
    return lastRoom;
  }
}
