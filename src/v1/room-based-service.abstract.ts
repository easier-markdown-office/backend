export default abstract class RoomBasedService<T> {
  protected roomBasedInfos: { [key: string]: T[] } = {};
}
