import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsString, MinLength } from 'class-validator';
import { default as RegisterDTOSuper } from '../../submodule-models/register.dto';
import {
  PASSWORD_MIN_LENGTH,
  USERNAME_MIN_LENGTH,
} from '../../submodule-models/constants';

export default class RegisterDTO extends RegisterDTOSuper {
  @ApiProperty({
    description: '...',
  })
  @MinLength(USERNAME_MIN_LENGTH)
  public username!: string;

  @ApiProperty({
    description: '...',
  })
  @IsEmail()
  public email!: string;

  @ApiProperty({
    description: '...',
  })
  @IsString()
  @MinLength(PASSWORD_MIN_LENGTH)
  public password!: string;

  public constructor(model: Partial<RegisterDTO>) {
    super(model);
  }
}
