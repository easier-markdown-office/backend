import { Injectable } from '@nestjs/common';
import UserDBM from '../models/schemas/user.schema';
import RegisterDTO from './models/register.dto';
import UserDocument from '../models/documents/user.document';

@Injectable()
export class AdminService {
  public async register(user: RegisterDTO): Promise<void> {
    await new UserDBM({
      username: user.username,
      email: user.email,
      password: user.password,
    } as UserDocument).save();
  }
}
