import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { AdminAuthGuard } from '../guards/admin-auth.guard';
import { AdminService } from './admin.service';
import RegisterDTO from './models/register.dto';
import ValidationPipe from '../pipes/validation.pipe';

@UseGuards(AdminAuthGuard)
@Controller('admin')
@ApiTags('admin')
@ApiBearerAuth()
export class AdminController {
  public constructor(private readonly service: AdminService) {}

  // @Header('Content-Type', 'text')
  @Post('user/create')
  @ApiBody({
    description: '...',
    type: RegisterDTO,
  })
  public async createUser(
    @Body(new ValidationPipe<RegisterDTO>(RegisterDTO))
    registerUser: RegisterDTO,
  ): Promise<void> {
    await this.service.register(registerUser);
  }
}
