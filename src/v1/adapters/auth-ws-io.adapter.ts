import { IoAdapter } from '@nestjs/platform-socket.io';
import { Request } from 'express';

// https://github.com/nestjs/nest/issues/882
export class AuthWsIoAdapter extends IoAdapter {
  public createIOServer(port: number, options?: any): any {
    options.allowRequest = async (
      request: Request,
      allowFunction: (
        errCode: string | number | null,
        success: boolean,
      ) => void,
    ) => {
      console.log('RES', request);
      return allowFunction(null, true);
      // return allowFunction(302, false);
    };
    return super.createIOServer(port, options);
  }
}
