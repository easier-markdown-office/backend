import { WsWrapperPipe } from './ws-wrapper.pipe';
import ParseObjectIdPipe from './parse-object-id.pipe';

describe('WsWrapperPipe', () => {
  it('should be defined', () => {
    expect(new WsWrapperPipe(new ParseObjectIdPipe())).toBeDefined();
  });
});
