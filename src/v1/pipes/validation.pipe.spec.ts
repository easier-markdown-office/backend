import ValidationPipe from './validation.pipe';
import ContributorInfoDTO from '../submodule-models/contributor-info.dto';

describe('ValidationPipe', () => {
  it('should be defined', () => {
    expect(
      new ValidationPipe<ContributorInfoDTO>(ContributorInfoDTO),
    ).toBeDefined();
  });
});
