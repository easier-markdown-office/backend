import { PipeTransform, Injectable, BadRequestException } from '@nestjs/common';
import { validate } from 'class-validator';

/**
 * This class represents a validation mechanism that aims to verify the data that is being transmitted
 * to the server by the users. It operates on the level of the request body and the data transfer objects /
 * other classes the server demands for the various operations.
 */
@Injectable()
export default class ValidationPipe<T> implements PipeTransform {
  /**
   * In the constructor of the ValidationPipe a partial of any type can be given to be verified
   * and checked.
   * @param model This is the Partial<Of any Type> that the pipe takes in and checks.
   */
  public constructor(private model: new (model: Partial<T>) => T) {}

  /**
   * This represents the primary operation of the pipe. It takes a given value and tries (in this case)
   * to check whether the value belonging to the model and the model form a valid object of type T.
   * @param value This represents the value that is being given to the function and can be any value of any type.
   * @return Returns either a valid model that has been formed out of the value or throws an error in case the validation failed.
   */
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async transform(value: any): Promise<T> {
    try {
      const model = new this.model(value); // throws an exception perhaps!
      const errors = await validate(model); // check the decorators
      if (errors.length > 0)
        throw new BadRequestException(
          'Validation failed for the properties: ' +
            errors.map((e) => e.property).join(', '),
        );

      return model;
    } catch (e) {
      if (e instanceof BadRequestException) throw e;

      throw new BadRequestException('Validation failed');
    }
  }
}
