import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { isDocumentNameValid } from '../submodule-models/utitlity.static';

@Injectable()
export class DocumentNamePipe implements PipeTransform<string, string> {
  public transform(value: string): string {
    if (!isDocumentNameValid(value))
      throw new BadRequestException('Invalid document name!');

    return value;
  }
}
