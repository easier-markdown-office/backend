import { PipeTransform, Injectable, BadRequestException } from '@nestjs/common';
import { Types } from 'mongoose';

@Injectable()
export default class ParseObjectIdPipe
  implements PipeTransform<any, Types.ObjectId> {
  public transform(value: any): Types.ObjectId {
    if (!value) throw new BadRequestException('Validation of the id failed');

    try {
      return new Types.ObjectId(value);
    } catch (e) {
      throw new BadRequestException('Validation of the id failed');
    }
  }
}
