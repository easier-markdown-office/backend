import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class WsWrapperPipe<TP extends PipeTransform, TR>
  implements PipeTransform<any, TR> {
  public constructor(private pipe: TP) {}

  public transform(value: any, metadata: ArgumentMetadata): TR {
    if (value.client && value.client.id) return value;

    return this.pipe.transform(value, metadata);
  }
}
