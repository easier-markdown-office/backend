import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { Types } from 'mongoose';

@ValidatorConstraint({ async: true })
export class IsMongoDBObjectIDConstraint
  implements ValidatorConstraintInterface {
  public validate(id: string) {
    try {
      new Types.ObjectId(id);
      return true;
    } catch (e) {
      return false;
    }
  }

  public defaultMessage(): string {
    return 'The ID is invalid!';
  }
}

export function IsMongoDBObjectID(validationOptions?: ValidationOptions) {
  // eslint-disable-next-line @typescript-eslint/ban-types
  return function (object: Object, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsMongoDBObjectIDConstraint,
    });
  };
}
