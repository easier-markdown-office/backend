import { MergingExceptionFilter } from './merging-exception.filter';

describe('Merging Exception Filter', () => {
  it('should be defined', () => {
    expect(new MergingExceptionFilter()).toBeDefined();
  });
});
