import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from '@nestjs/common';
import { WsException } from '@nestjs/websockets';
import { Socket } from 'socket.io';

@Catch()
export class WsTransformerFilter<T extends Error> implements ExceptionFilter {
  public catch(exception: T, host: ArgumentsHost) {
    const client = host.switchToWs().getClient<Socket>();
    console.log(client?.id);
    if (exception instanceof HttpException) {
      console.log(exception.getStatus(), exception.message);
    } else if (exception instanceof WsException) {
      console.log(exception.message);
    } else {
      console.log('internal server error!', exception.message);
    }
    console.log('catch error');
    return 'test';
  }
}
