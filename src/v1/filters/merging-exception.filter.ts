import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { Socket } from 'socket.io';
import { ERROR_EVENTS } from '../submodule-models/gateway-addresses.constants';

@Catch()
export class MergingExceptionFilter<T> implements ExceptionFilter {
  public catch(exception: T, host: ArgumentsHost) {
    const client = host.switchToWs().getClient<Socket>();
    client.emit(ERROR_EVENTS.merging);
  }
}
