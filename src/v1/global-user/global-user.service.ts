import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import LoginDataDTO from '../user/models/login-data.dto';
import LoginResultDTO from '../user/models/login-result.dto';
import validator from 'validator';
import UserDBM from '../models/schemas/user.schema';
import DocumentDTO from '../models/document.dto';
import DocumentDBM from '../models/schemas/document.schema';
import UserDocument from '../models/documents/user.document';
import { ERoom } from '../models/room.enum';
import { sign as JWTSign } from 'jsonwebtoken';
import { jwtPW } from '../../../configs/config.json';
import { Subject } from 'rxjs';
import { EventArgs } from '../document/models/event-args.model';
import UserDTO from '../user/models/user.dto';

@Injectable()
export class GlobalUserService {
  public newConnection = new Subject();
  public roomChanged = new Subject<EventArgs>();
  public toLoadDocuments = new Subject<
    {
      id: string;
      name: string;
      lines: string[];
    }[]
  >();

  /**
   * This function serves to create a token for a user on login or for the tempUser on creation
   * of the baseUser object.
   * @param user This represents the user the token will be assigned to and is part of the tokens creation.
   * @param userAgent This represents the userAgent information of the user the token is created for.
   * @return Returns the newly formed token.
   */
  public static async createLoginToken(
    user: UserDocument,
    userAgent: string,
  ): Promise<string> {
    const token = JWTSign({ _id: user._id.toString(), userAgent }, jwtPW);
    user.tokens.push(token);
    await user.updateOne({ $push: { tokens: token } });

    return token;
  }

  /**
   * This functions handles the login of a user. It takes the information from a user and tries to
   * verify it in order for the user to access his profile on the server.
   * @param usernameOrEmail Contains the username or user-email-address that the user provides the server with when logging in.
   * @param password Contains the password that the user provides the server with when logging in.
   * @param userAgent This represents the userAgent information which is being sent via the request.
   * @return Returns the encoded JSON token as a string.
   */
  public async login(
    { user: usernameOrEmail, password }: LoginDataDTO,
    userAgent: string,
  ): Promise<LoginResultDTO> {
    try {
      let user;
      if (validator.isEmail(usernameOrEmail)) {
        user = await UserDBM.findByEmailAndPassword(usernameOrEmail, password);
      } else {
        user = await UserDBM.findByUsernameAndPassword(
          usernameOrEmail,
          password,
        );
      }

      return {
        ...(await this.getDocuments(user)),
        token: await GlobalUserService.createLoginToken(user, userAgent),
      } as LoginResultDTO;
    } catch (error) {
      if (
        error instanceof BadRequestException ||
        error instanceof InternalServerErrorException
      ) {
        throw error;
      } else if (error instanceof Error) {
        throw new BadRequestException('Wrong email, username or password');
      }
      throw new InternalServerErrorException('Something went wrong!');
    }
  }

  public async getDocuments(user: UserDocument): Promise<UserDTO> {
    const documents: DocumentDTO[] = [];
    const collaboDocs: DocumentDTO[] = [];

    for (const doc of await DocumentDBM.find({ author: user.id })) {
      documents.push(
        new DocumentDTO({
          name: doc.name,
          id: doc.id.toString(),
          modDate: doc.modificationDate,
        }),
      );
    }

    for (const doc of await DocumentDBM.find({ contributors: user.id })) {
      collaboDocs.push(
        new DocumentDTO({
          name: doc.name,
          id: doc.id.toString(),
          modDate: doc.modificationDate,
        }),
      );
    }

    return new UserDTO({ username: user.username, documents, collaboDocs });
  }

  public async getRooms(user: UserDocument): Promise<string[]> {
    const rooms: string[] = [];
    const toLoadDocuments: { id: string; name: string; lines: string[] }[] = [];
    rooms.push(`${ERoom.user}${user.id.toString()}`);

    for (const doc of await DocumentDBM.find({ author: user.id })) {
      toLoadDocuments.push({ id: doc.id, name: doc.name, lines: doc.lines });
      rooms.push(`${ERoom.document}${doc.id.toString()}`);
    }

    for (const doc of await DocumentDBM.find({ contributors: user.id })) {
      toLoadDocuments.push({ id: doc.id, name: doc.name, lines: doc.lines });
      rooms.push(`${ERoom.collaborator}${doc.id.toString()}`);
    }

    this.toLoadDocuments.next(toLoadDocuments);
    return rooms;
  }

  public async logout(user: UserDocument, token: string): Promise<void> {
    await user.updateOne({ tokens: user.tokens.filter((t) => t !== token) });
  }
}
