import { Module } from '@nestjs/common';
import { GlobalUserService } from './global-user.service';

@Module({
  providers: [GlobalUserService],
  exports: [GlobalUserService],
})
export class GlobalUserModule {}
