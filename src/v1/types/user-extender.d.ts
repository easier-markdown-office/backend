import UserDocument from '../models/documents/user.document';
import DocumentDocument from '../models/documents/document.document';

declare global {
  interface UserInformation {
    user?: UserDocument;
    selectedDocument?: DocumentDocument;
    checkedEmailAddressUser?: UserDocument;
    token?: string;
    userAgent: string;
  }
}

export {};
