import { Test, TestingModule } from '@nestjs/testing';
import { DocumentGateway } from './document.gateway';
import { DocumentWsService } from './document-ws.service';
import { DocumentHttpService } from './document-http.service';
import { DocumentBridgeService } from './document-bridge.service';
import { DocumentController } from './document.controller';
import { GlobalUserModule } from '../global-user/global-user.module';

describe('Document Gateway', () => {
  let gateway: DocumentGateway;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DocumentHttpService,
        DocumentBridgeService,
        DocumentWsService,
        DocumentGateway,
      ],
      controllers: [DocumentController],
      imports: [GlobalUserModule],
    }).compile();

    gateway = module.get<DocumentGateway>(DocumentGateway);
  });

  it('should be defined', () => {
    expect(gateway).toBeDefined();
  });
});
