import { TextDifferent } from '../submodule-models/text-different.model';
import { sumUpLineModifications } from '../submodule-models/utitlity.static';
import { DifferentRangeResult } from './merging/models/different-range-result.model';
import { EChangeType } from '../submodule-models/change-type.enum';
import { HistoryItem } from './models/history-item.model';

export function getDiffsInRange(
  startPos: number,
  endPos: number,
  diffs: TextDifferent[],
): DifferentRangeResult[] {
  const result: DifferentRangeResult[] = [];

  let currentOffset = 0;
  for (let i = 0; i < diffs.length && currentOffset < endPos; ++i) {
    const diff = diffs[i];
    if (Array.isArray(diff)) {
      const [diffType, diffItem] = diff;
      let add = false;
      if (diffType === EChangeType.insertion) {
        add = currentOffset >= startPos;
      }

      if (diffType === EChangeType.deletion) {
        add =
          currentOffset >= startPos ||
          currentOffset + diffItem.length <= endPos;
      }

      if (add) {
        result.push({
          index: i,
          offset: currentOffset,
          matchComplete: currentOffset + diffItem.length <= endPos,
          type: diffType,
        });
      }

      if (diffType === EChangeType.insertion) currentOffset += diffItem.length;
    } else {
      currentOffset += diff;
    }
  }

  return result;
}

export function sumUpHistoryItem(historyItems: HistoryItem[]): HistoryItem[] {
  for (let i = 0; i < historyItems.length; ++i) {
    historyItems[i].changes = sumUpLineModifications(historyItems[i].changes);
  }
  return historyItems;
}
