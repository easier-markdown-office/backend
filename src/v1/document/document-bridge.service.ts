import { BadRequestException, Injectable } from '@nestjs/common';
import DocumentDBM from '../models/schemas/document.schema';
import { DocumentHttpService } from './document-http.service';
import { DocumentWsService } from './document-ws.service';
import DocumentDTO from '../submodule-models/document.dto';
import { ERoom } from '../models/room.enum';
import { DOCUMENT_EVENTS } from '../submodule-models/gateway-addresses.constants';
import { Server } from 'socket.io';
import UtilityService from '../utility.service';
import UserDocument from '../models/documents/user.document';
import DocumentDocument from '../models/documents/document.document';
import ContributorInfoDTO from '../submodule-models/contributor-info.dto';

@Injectable()
export class DocumentBridgeService {
  private _wsServer: Server | undefined;

  public set wsServer(server: Server) {
    this._wsServer = server;
  }

  public constructor(
    private readonly httpService: DocumentHttpService,
    private readonly wsService: DocumentWsService,
  ) {}

  public async createNewDocumentAndEmitEvents(
    docName: string,
    userID: string,
  ): Promise<string> {
    const newID = await this.createNewDocument(docName, userID);
    const newDocu = new DocumentDTO({ id: newID, name: docName });

    this._wsServer
      ?.in(`${ERoom.user}${userID}`)
      .emit(DOCUMENT_EVENTS.created, newDocu);
    return newID;
  }

  public async createNewDocument(
    name: string,
    authorID: string,
  ): Promise<string> {
    const newDoc = new DocumentDBM({ name, author: authorID });
    await newDoc.save();
    this.wsService.addExistingDocument(newDoc.id.toString(), name);

    return newDoc.id.toString();
  }

  public async deleteDocumentAndEmitEvents(
    docID: string,
    userID: string,
  ): Promise<void> {
    await this.deleteDocument(docID);
    this.wsService.removeDocument(docID);

    if (!this._wsServer) return;

    this._wsServer
      ?.in(`${ERoom.user}${userID}`)
      .emit(DOCUMENT_EVENTS.removed, docID);
    UtilityService.getSocketsFromRoomName(
      this._wsServer,
      `${ERoom.user}${userID}`,
    ).forEach((c) => c.leave(`${ERoom.document}${docID}`));

    this._wsServer
      ?.in(`${ERoom.collaborator}${docID}`)
      .emit(DOCUMENT_EVENTS.collaboRemoved, docID);
    UtilityService.getSocketsFromRoomName(
      this._wsServer,
      `${ERoom.collaborator}${docID}`,
    ).forEach((c) => c.leave(`${ERoom.collaborator}${docID}`));
  }

  public async deleteDocument(docID: string): Promise<void> {
    await DocumentDBM.deleteOne({ _id: docID });
  }

  public async renameDocumentAndEmitEvents(
    docID: string,
    userID: string,
    newName: string,
  ): Promise<void> {
    await this.renameDocument(docID, newName);
    this.wsService.renameDocument(docID, newName);

    this._wsServer
      ?.in(`${ERoom.user}${userID}`)
      .emit(
        DOCUMENT_EVENTS.renamed,
        new DocumentDTO({ id: docID, name: newName }),
      );

    this._wsServer
      ?.in(`${ERoom.collaborator}${docID}`)
      .emit(
        DOCUMENT_EVENTS.collaboRenamed,
        new DocumentDTO({ id: docID, name: newName }),
      );
  }

  public async renameDocument(docID: string, newName: string): Promise<void> {
    await DocumentDBM.updateOne({ _id: docID }, { name: newName });
  }

  public async inviteAndEmitEvents(
    doc: DocumentDocument,
    invitedUser: UserDocument,
    user: UserDocument,
  ) {
    if (invitedUser.email === user.email)
      throw new BadRequestException('You can not invite yourself.');

    if (doc.contributors.find((c) => c.email === invitedUser.email))
      throw new BadRequestException(
        'The contributor is already a part of the document!',
      );

    await doc.updateOne({ $push: { contributors: [invitedUser] } } as any);

    if (!this._wsServer) return;
    const eventData = new DocumentDTO({
      name: doc.name,
      id: doc.id.toString(),
      modDate: doc.modificationDate,
    });
    UtilityService.getSocketsFromRoomName(
      this._wsServer,
      `${ERoom.user}${invitedUser.id.toString()}`,
    ).forEach((c) => {
      c.join(`${ERoom.collaborator}${doc.id.toString()}`);
      this._wsServer?.in(c.id).emit(DOCUMENT_EVENTS.collaboCreated, eventData);
    });
  }

  public async dischargeAndEmitEvents(
    doc: DocumentDocument,
    dischargedUser: UserDocument,
    user: UserDocument,
  ) {
    if (dischargedUser.email === user.email)
      throw new BadRequestException('You can not discharge yourself!');

    if (doc.contributors.find((c) => c.email !== dischargedUser.email))
      throw new BadRequestException(
        'The contributor is not a part of the document already!',
      );

    const newContri = doc.contributors.filter(
      (c) => c.email !== dischargedUser.email,
    );
    await doc.updateOne({ contributors: newContri });

    if (!this._wsServer) return;
    UtilityService.getSocketsFromRoomName(
      this._wsServer,
      `${ERoom.user}${dischargedUser.id.toString()}`,
    ).forEach((c) => {
      c.leave(`${ERoom.collaborator}${doc.id.toString()}`);
      this._wsServer
        ?.in(c.id)
        .emit(DOCUMENT_EVENTS.collaboRemoved, doc.id.toString());
    });
  }

  public async leaveAndEmitEvents(
    doc: DocumentDocument,
    user: UserDocument,
  ): Promise<void> {
    console.log(doc.author.id, user.id);
    if (doc.author === user.id)
      throw new BadRequestException('You can not leave your own document!');

    const newContri = doc.contributors.filter((c) => c.email !== user.email);
    await doc.updateOne({ contributors: newContri });

    if (!this._wsServer) return;
    UtilityService.getSocketsFromRoomName(
      this._wsServer,
      `${ERoom.user}${user.id.toString()}`,
    ).forEach((c) => {
      c.leave(`${ERoom.collaborator}${doc.id.toString()}`);
      this._wsServer
        ?.in(c.id)
        .emit(DOCUMENT_EVENTS.collaboRemoved, doc.id.toString());
    });
  }

  public async getContributors(
    document: DocumentDocument,
  ): Promise<ContributorInfoDTO[]> {
    await document.populate('contributors').execPopulate();
    return document.contributors.map(
      (c) => new ContributorInfoDTO({ username: c.username, email: c.email }),
    );
  }
}
