import { Module } from '@nestjs/common';
import { DocumentWsService } from './document-ws.service';
import { DocumentGateway } from './document.gateway';
import { DocumentController } from './document.controller';
import { DocumentHttpService } from './document-http.service';
import { DocumentBridgeService } from './document-bridge.service';
import { GlobalUserModule } from '../global-user/global-user.module';
import { WsAuthGuard } from '../guards/ws-auth.guard';
import { WsDocumentOwnerGuard } from '../guards/ws-document-owner.guard';

@Module({
  providers: [
    DocumentGateway,
    DocumentWsService,
    DocumentHttpService,
    DocumentBridgeService,
    WsAuthGuard,
    WsDocumentOwnerGuard,
  ],
  controllers: [DocumentController],
  imports: [GlobalUserModule],
})
export class DocumentModule {}
