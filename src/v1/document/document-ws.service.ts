import { readFile } from 'fs';
import { join } from 'path';
import { inspect, promisify } from 'util';
import { Injectable, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { MarkdownFile } from './models/markdown-file.model';
import {
  convertJSONClassesToRealClasses,
  mergeLines,
} from '../submodule-models/utitlity.static';
import UpdateTextDTO from '../submodule-models/update-text.dto';
import { HistoryItem } from './models/history-item.model';
import { CompleteTextDTO } from '../submodule-models/complete-text.dto';
import { TextChangedDTO } from '../submodule-models/text-changed.dto';
import { AdjustClientTextDTO } from '../submodule-models/adjust-client-text.dto';
import LineModification from '../submodule-models/line-modification.abstract';
import { LineInsertionDTO } from '../submodule-models/line-insertion.dto';
import { LineDeletionDTO } from '../submodule-models/line-deletion.dto';
import AdjusterHelper from './adjuster.helper';
import MergingHelper from './merging.helper';
import { LineContentModificationDTO } from '../submodule-models/line-content-modification.dto';
import { TextDifferent } from '../submodule-models/text-different.model';
import DocumentDBM from '../models/schemas/document.schema';

const CLEAR_HISTORY_AFTER = 1000 * 60 * 5; // time in ms => 5min
const CLEAR_HISTORY_INTERVAL = 1000 * 60; // in ms => 1min
const SAVE_DOCUMENTS_INTERVAL = 1000 * 60; // in ms => 1min

const LINE_SPLITTER = /[\r?\n]/;

const START_TEMPLATE_PATH = join(__dirname, '../../../templates/start.md');

@Injectable()
export class DocumentWsService implements OnModuleInit, OnModuleDestroy {
  private _markdownFiles: MarkdownFile[] = [];
  private _historyCleanerTimer: NodeJS.Timeout | undefined;
  private _saveTimer: NodeJS.Timeout | undefined;
  private _startTemplate = '';
  private _lastSaveTime = Date.now();

  private static invertDiffs(diffs: TextDifferent[]): TextDifferent[] {
    const result: TextDifferent[] = [];
    diffs.forEach((diff) => {
      // * -1 = invert type (insertion to deletion and deletion to insertion)
      if (Array.isArray(diff)) result.push([diff[0] * -1, diff[1]]);
      else result.push(diff);
    });

    return result;
  }

  private static invertModifications(
    mods: LineModification[],
  ): LineModification[] {
    const result: LineModification[] = [];

    mods.forEach((mod) => {
      if (mod instanceof LineInsertionDTO)
        result.push(new LineDeletionDTO({ index: mod.index }));
      else if (mod instanceof LineDeletionDTO)
        result.push(new LineInsertionDTO({ index: mod.index }));
      else if (mod instanceof LineContentModificationDTO)
        result.push(
          new LineContentModificationDTO({
            index: mod.index,
            changes: this.invertDiffs(mod.changes),
          }),
        );
      else result.push(mod);
    });

    return result;
  }

  public async onModuleInit(): Promise<void> {
    await this.init();
    this._historyCleanerTimer = setInterval(
      this.historyCleaner.bind(this),
      CLEAR_HISTORY_INTERVAL,
    );

    this._saveTimer = setInterval(
      this.saveDocuments.bind(this),
      SAVE_DOCUMENTS_INTERVAL,
    );
  }

  public async onModuleDestroy(): Promise<void> {
    if (this._historyCleanerTimer) clearInterval(this._historyCleanerTimer);
    if (this._saveTimer) clearInterval(this._saveTimer);
  }

  public getCompleteText(id: string): CompleteTextDTO {
    const markdownFile = this.getMarkdownFileFromID(id);
    if (!markdownFile) throw new Error('Invalid markdown id. Not found!');

    let lines = [...markdownFile.lines];
    const hash =
      markdownFile.history.length > 0
        ? markdownFile.history[markdownFile.history.length - 1].hash
        : markdownFile.startHash;
    for (const historyItem of markdownFile.history) {
      lines = mergeLines(lines, historyItem.changes);
    }

    return { lines, hash };
  }

  public changeText(
    changedText: UpdateTextDTO,
    clientID: string,
    id: string,
  ): { broadcast: TextChangedDTO; clientResponse: AdjustClientTextDTO } {
    const markdownFile = this.getMarkdownFileFromID(id);
    if (!markdownFile) throw new Error('Invalid markdown id. Not found!');

    console.log(
      'lastHash',
      markdownFile.history.length > 0
        ? markdownFile.history[markdownFile.history.length - 1].hash
        : markdownFile.startHash,
    );
    console.log('is based on', changedText.basedOn);
    console.log('current text', this.getCompleteText(id).lines);

    // ws sent a json, no classes.
    // so convert the json things back to classes
    changedText.changes = convertJSONClassesToRealClasses(changedText.changes); // .sort((m1, m2) => m1.index - m2.index);

    const oldDiff = changedText.changes.map((mod) => mod.clone()); // make copy of the original
    const adjustAndMergeResult = this.adjustAndMerge(
      markdownFile,
      changedText,
      clientID,
    );
    changedText.changes = adjustAndMergeResult.changes;

    // each missing commit has an array of modifications
    const missedChangesClient: LineModification[][] = [];
    if (adjustAndMergeResult.mergeIndex >= 0) {
      missedChangesClient.push(DocumentWsService.invertModifications(oldDiff));

      for (const historyItem of [...markdownFile.history].splice(
        adjustAndMergeResult.mergeIndex,
      )) {
        missedChangesClient.push(historyItem.changes);
      }
      missedChangesClient.push(adjustAndMergeResult.changes);
    }

    const newHistoryItem = {
      changes: changedText.changes,
      hash: Date.now(),
      author: clientID,
    } as HistoryItem;

    console.log('new hash:', newHistoryItem.hash);
    markdownFile.history.push(newHistoryItem);

    return {
      broadcast: { ...newHistoryItem, checksum: '' },
      clientResponse: {
        missedMods: missedChangesClient,
        hash: newHistoryItem.hash,
        checksum: '',
      },
    };
  }

  public addExistingDocument(id: string, name: string, text?: string[]): void {
    const markdownFile = this.getMarkdownFileFromID(id);
    if (markdownFile)
      throw new Error('Markdown File with the id aka name exists already!');

    if (!text) text = this._startTemplate.split(LINE_SPLITTER);

    this._markdownFiles.push({
      id,
      name,
      lines: text,
      startHash: Date.now(),
      history: [] as HistoryItem[],
      isClearing: false,
    } as MarkdownFile);
  }

  public removeDocument(id: string): void {
    this._markdownFiles = this._markdownFiles.filter((m) => m.id !== id);
  }

  public renameDocument(id: string, newName: string): void {
    for (const markdownFile of this._markdownFiles) {
      if (markdownFile.id !== id) continue;

      markdownFile.name = newName;
    }
  }

  public loadMarkdown(id: string, name: string, text?: string[]): void {
    try {
      this.addExistingDocument(id, name, text);
    } catch (e) {
      // ignore exception
    }
  }

  private async init(): Promise<void> {
    this._startTemplate = (
      await promisify(readFile)(START_TEMPLATE_PATH)
    ).toString();

    for (const document of await DocumentDBM.find({})) {
      this.loadMarkdown(document.id, document.name, document.lines);
    }

    this._lastSaveTime = Date.now();
  }

  private adjustAndMerge(
    markdownFile: MarkdownFile,
    changedText: UpdateTextDTO,
    clientID: string,
  ): { mergeIndex: number; changes: LineModification[] } {
    let mustMerge = false;
    const historyIndex = markdownFile.history.findIndex(
      (h) => h.hash === changedText.basedOn,
    );
    // check if changes based NOT on the current state (based on an old state)
    if (
      // aka historyIndex !== markdownFile.history.length
      (historyIndex >= 0 && historyIndex + 1 < markdownFile.history.length) ||
      (historyIndex < 0 && markdownFile.history.length > 0)
    ) {
      mustMerge = true;

      console.log('must merge/adjust');
      console.log('old diff', inspect(changedText.changes, false, null, true));
      const adjuster = new AdjusterHelper(clientID, markdownFile);
      const adjusterResult = adjuster.adjust(
        historyIndex + 1,
        changedText.changes,
      );
      changedText.changes = adjusterResult.toAdjustModifications;

      const merger = new MergingHelper(clientID, markdownFile);
      changedText.changes = merger.merge(
        adjusterResult.history,
        changedText.changes,
      );
      console.log('new diff', inspect(changedText.changes, false, null, true));
    }

    return {
      mergeIndex: mustMerge ? historyIndex + 1 : -1,
      changes: changedText.changes,
    };
  }

  private getMarkdownFileFromID(id: string): MarkdownFile | undefined {
    return this._markdownFiles.find((mf) => mf.id === id);
  }

  private historyCleaner(): void {
    const currentDate = Date.now();

    for (const markdownFile of this._markdownFiles) {
      if (markdownFile.history.length > 0) {
        markdownFile.isClearing = true;
        while (markdownFile.history.length > 0) {
          if (currentDate - markdownFile.history[0].hash < CLEAR_HISTORY_AFTER)
            break;

          markdownFile.lines = mergeLines(
            markdownFile.lines,
            markdownFile.history[0].changes,
          );
          markdownFile.startHash = markdownFile.history[0].hash;
          markdownFile.history.splice(0, 1);
        }
        markdownFile.isClearing = false;
        console.info('History was cleaned up');
      }
    }
  }

  private async saveDocuments(): Promise<void> {
    for (const markdownFile of this._markdownFiles) {
      const lastHash =
        markdownFile.history.length > 0
          ? markdownFile.history[markdownFile.history.length - 1].hash
          : markdownFile.startHash;

      if (lastHash > this._lastSaveTime) {
        try {
          await DocumentDBM.updateOne(
            { _id: markdownFile.id },
            {
              lines: this.getCompleteText(markdownFile.id).lines,
              modificationDate: lastHash,
            },
          );
        } catch (e) {
          console.warn('Get text or save failed!', e);
        }
      }
    }

    this._lastSaveTime = Date.now();
  }
}
