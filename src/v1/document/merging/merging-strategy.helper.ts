import { Diff } from 'diff-match-patch';
import { TextDifferent } from '../../submodule-models/text-different.model';
import { EChangeType } from '../../submodule-models/change-type.enum';
import { DifferentRangeResult } from './models/different-range-result.model';
import { MergingStrategyResult } from './models/merging-strategy-result.model';
import { strategyDeletionAndDeletion } from './merging-strategy-deletions.helper';
import {
  specialStrategyInsertionAndInsertionOnText,
  strategyInsertionAndInsertion,
} from './merging-strategy-insertions.helper';
import { MergingStrategyParameter } from './models/merging-strategy-parameter.model';
import { inspect } from 'util';

type StrategyXOnYParams = {
  hDiff: Diff;
  paramBase: MergingStrategyParameter;
  isEndIndexOfNewDiff: boolean;
  lastChangeItemIndex: number;
  lastChangeItem: Diff;
};

export default class MergingStrategyHelper {
  private readonly _clientID: string;
  private readonly _toMergeDiff: Diff;
  private readonly _hDiffs: TextDifferent[];

  private _currentPos = 0;
  private _newToMergeDiff: TextDifferent[] = [];
  private _hAuthorID = '';

  public constructor(
    clientID: string,
    toMergeDiff: Diff,
    hDiffs: TextDifferent[],
  ) {
    this._clientID = clientID;
    this._toMergeDiff = toMergeDiff;
    this._hDiffs = hDiffs;
  }

  public applyMergeStrategy(
    currentPos: number,
    hChanges: DifferentRangeResult[],
    hAuthorID: string,
    isEndIndexOfNewDiff: boolean,
  ): MergingStrategyResult {
    const deleteIndices: number[] = [];
    const newDiffType: EChangeType = this._toMergeDiff[0];

    this._newToMergeDiff = [this._toMergeDiff] as TextDifferent[];
    this._currentPos = currentPos;
    this._hAuthorID = hAuthorID;

    for (const hChange of hChanges) {
      const lastChangeItemIndex = this._newToMergeDiff.findIndex((diff) =>
        Array.isArray(diff),
      );
      if (lastChangeItemIndex < 0) break;
      const lastChangeItem = this._newToMergeDiff[lastChangeItemIndex] as Diff;

      const hDiff = this._hDiffs[hChange.index];
      if (!Array.isArray(hDiff)) continue;

      const hDiffType = hDiff[0];
      const paramBase = {
        currentPos: this._currentPos,
        hDiffs: this._hDiffs,
        hChange,
      } as MergingStrategyParameter;

      if (newDiffType === EChangeType.insertion) {
        this.strategyInsertionOnX({
          hDiff,
          paramBase,
          isEndIndexOfNewDiff,
          lastChangeItemIndex,
          lastChangeItem,
        });
      } else if (newDiffType === EChangeType.deletion) {
        if (hDiffType === EChangeType.deletion) {
          const strategyResult = strategyDeletionAndDeletion(
            hChange,
            hDiff,
            this._currentPos,
            lastChangeItem,
          );

          if (strategyResult.deleteID) deleteIndices.push(hChange.index);

          this._currentPos = strategyResult.currentPos;
          this.handleSingleDiffReturn(
            lastChangeItemIndex,
            strategyResult.newToMergeDiff,
          );
        }
      }
    }

    return {
      newOffset: this._currentPos,
      newToMergeDiff: this._newToMergeDiff,
      deleteIndices,
    };
  }

  private handleSingleDiffReturn(index: number, diff: Diff | undefined) {
    if (!diff) this._newToMergeDiff.splice(index, 1);
    else this._newToMergeDiff[index] = diff;
  }

  private strategyInsertionOnX({
    hDiff,
    paramBase,
    isEndIndexOfNewDiff,
    lastChangeItemIndex,
    lastChangeItem,
  }: StrategyXOnYParams): void {
    const [hDiffType, hDiffItem] = hDiff;
    if (hDiffType === EChangeType.insertion) {
      if (this._hAuthorID === this._clientID) {
        const strategyResult = specialStrategyInsertionAndInsertionOnText({
          ...paramBase,
          hDiff,
          toMergeDiff: lastChangeItem,
          isEndIndexOfNewDiff,
          hDiffItemLen: hDiffItem.length,
        });

        console.log(
          'special merging, because newer history item of the same author',
          inspect(strategyResult.newToMergeDiff, false, null, true),
        );
        this._currentPos = strategyResult.currentPos;
        if (strategyResult.newToMergeDiffArray) {
          this._newToMergeDiff.splice(
            lastChangeItemIndex,
            1,
            ...strategyResult.newToMergeDiffArray,
          );
        } else {
          this.handleSingleDiffReturn(
            lastChangeItemIndex,
            strategyResult.newToMergeDiff,
          );
        }
      } else {
        this._currentPos = strategyInsertionAndInsertion({
          ...paramBase,
          isEndIndexOfNewDiff,
          hDiffItemLen: hDiffItem.length,
        });
      }
    }
  }
}
