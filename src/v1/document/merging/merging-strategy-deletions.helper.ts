import { DifferentRangeResult } from './models/different-range-result.model';
import { Diff } from 'diff-match-patch';
import { EChangeType } from '../../submodule-models/change-type.enum';
// import { MergingStrategyParameter } from './models/merging-strategy-parameter.model';

/*
type DeletionOnDeletionParam = MergingStrategyParameter & {
  toMergeDiff: Diff;
};
 */

export function strategyDeletionAndDeletion(
  hChange: DifferentRangeResult,
  hDiff: Diff,
  currentPos: number,
  toMergeDiff: Diff,
): {
  currentPos: number;
  newToMergeDiff: Diff | undefined;
  deleteID: boolean;
} {
  const [, hDiffItem] = hDiff;
  const [, toMergeDiffItem] = toMergeDiff;
  let newToMergeDiff: Diff | undefined;
  let deleteID = false;

  // toMergeDiff is inside a deletion
  if (
    currentPos >= hChange.offset &&
    currentPos + toMergeDiffItem.length <= hChange.offset + hDiffItem.length
  ) {
    newToMergeDiff = undefined; // delete toMergeDiff
  }
  // toMergeDiff starts before historyItem but ends inside the historyItem
  else if (
    currentPos < hChange.offset &&
    currentPos + toMergeDiffItem.length <= hChange.offset + hDiffItem.length &&
    currentPos + toMergeDiffItem.length > hChange.offset
  ) {
    newToMergeDiff = [
      EChangeType.deletion,
      toMergeDiff[1].substring(0, hChange.offset - currentPos),
    ];
    deleteID = true;
  }
  // toMergeDiff starts inside historyItem but ends after the historyItem
  else if (
    currentPos > hChange.offset &&
    currentPos < hChange.offset + hDiffItem.length &&
    currentPos + toMergeDiffItem.length > hChange.offset + hDiffItem.length
  ) {
    newToMergeDiff = [
      EChangeType.deletion,
      toMergeDiff[1].substring(
        hDiffItem.length - (currentPos - hChange.offset),
      ),
    ];
    currentPos -= currentPos - hChange.offset;
  }
  // historyItem is part of toMergeItem
  else if (
    currentPos <= hChange.offset &&
    currentPos + toMergeDiffItem.length >= hChange.offset + hDiffItem.length
  ) {
    const startOffset = hChange.offset - currentPos;
    const strToHistoryItem =
      hChange.offset > currentPos
        ? toMergeDiffItem.substring(0, startOffset)
        : '';
    const endStr =
      currentPos + toMergeDiffItem.length > hChange.offset + hDiffItem.length
        ? toMergeDiffItem.substring(startOffset + hDiffItem.length)
        : '';
    newToMergeDiff = [EChangeType.deletion, `${strToHistoryItem}${endStr}`];
  } else {
    throw new Error('MERGE CONFLICT!');
  }

  return { currentPos, newToMergeDiff, deleteID };
}
