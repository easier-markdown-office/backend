import { Diff } from 'diff-match-patch';
import { EChangeType } from '../../submodule-models/change-type.enum';
import { MergingStrategyParameter } from './models/merging-strategy-parameter.model';
import { TextDifferent } from '../../submodule-models/text-different.model';

type InsertionParam = MergingStrategyParameter & {
  isEndIndexOfNewDiff: boolean;
  hDiffItemLen: number;
};

type SpecialInsertionOnInsertionParam = MergingStrategyParameter &
  InsertionParam & {
    hDiff: Diff;
    toMergeDiff: Diff;
  };

export function strategyInsertionAndInsertion(param: InsertionParam): number {
  // merge conflict: if insertion + insertion at the same position
  //   exceptions: - at the beginning of the document
  //               - at the end of the document
  if (
    param.hChange.offset === param.currentPos &&
    param.currentPos !== 0 &&
    !(
      param.isEndIndexOfNewDiff &&
      param.hChange.index === param.hDiffs.length - 1
    )
  ) {
    throw new Error('MERGE CONFLICT!');
  } else {
    param.currentPos += param.hDiffItemLen;
  }

  return param.currentPos;
}

function isInsertionPositionIrrelevant(
  toMergeItem: string,
  hDiffItem: string,
): boolean {
  if (toMergeItem.length === 2 && hDiffItem.length === 1) {
    return toMergeItem[0] === toMergeItem[1] && toMergeItem[1] === hDiffItem[0];
  }

  return false;
}

export function specialStrategyInsertionAndInsertionOnText(
  param: SpecialInsertionOnInsertionParam,
): {
  currentPos: number;
  newToMergeDiffArray?: TextDifferent[];
  newToMergeDiff: Diff | undefined;
} {
  let newToMergeDiff: Diff | undefined = param.toMergeDiff;
  let newToMergeDiffArray: undefined | TextDifferent[];
  const [, hDiffItem] = param.hDiff;
  const [, toMergeItem] = param.toMergeDiff;

  let indexOfhItemInToMergeItem = toMergeItem.lastIndexOf(hDiffItem);
  if (
    param.currentPos >= param.hChange.offset &&
    param.currentPos <= param.hChange.offset + hDiffItem.length &&
    indexOfhItemInToMergeItem >= 0
  ) {
    if (isInsertionPositionIrrelevant(toMergeItem, hDiffItem))
      indexOfhItemInToMergeItem = 0;

    if (indexOfhItemInToMergeItem !== 0) {
      newToMergeDiff = [
        EChangeType.insertion,
        toMergeItem.substring(0, indexOfhItemInToMergeItem),
      ];
    } else {
      newToMergeDiff = [
        EChangeType.insertion,
        toMergeItem.substring(indexOfhItemInToMergeItem + hDiffItem.length),
      ];
      param.currentPos += indexOfhItemInToMergeItem + hDiffItem.length;
    }
  } else {
    param.currentPos = strategyInsertionAndInsertion(param);
  }

  return {
    currentPos: param.currentPos,
    newToMergeDiffArray,
    newToMergeDiff,
  };
}
