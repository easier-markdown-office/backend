import { EChangeType } from '../../../submodule-models/change-type.enum';

export type DifferentRangeResult = {
  index: number;
  offset: number;
  type: EChangeType;
  matchComplete: boolean;
};
