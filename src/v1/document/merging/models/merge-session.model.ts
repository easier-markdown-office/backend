import { TextDifferent } from '../../../submodule-models/text-different.model';

export type MergeSession = {
  /** Specifies the current position/offset of the text of toMergeDiffs. */
  currentPosOfToMergeDiff: number;
  /** */
  toMergeIndex: number;

  toMergeChanges: TextDifferent[];
};
