import { TextDifferent } from '../../../submodule-models/text-different.model';

export type MergingStrategyResult = {
  newOffset: number;
  newToMergeDiff: TextDifferent[];
  deleteIndices: number[];
};
