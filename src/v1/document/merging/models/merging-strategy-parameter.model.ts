import { TextDifferent } from '../../../submodule-models/text-different.model';
import { DifferentRangeResult } from './different-range-result.model';

export type MergingStrategyParameter = {
  currentPos: number;
  hDiffs: TextDifferent[];
  hChange: DifferentRangeResult;
};
