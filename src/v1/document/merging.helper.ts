import { Diff } from 'diff-match-patch';
import {
  sumUpLineModifications,
  sumUpTextDiffs,
} from '../submodule-models/utitlity.static';
import LineModification from '../submodule-models/line-modification.abstract';
import { LineContentModificationDTO } from '../submodule-models/line-content-modification.dto';
import { TextDifferent } from '../submodule-models/text-different.model';
import { EChangeType } from '../submodule-models/change-type.enum';
import { MarkdownFile } from './models/markdown-file.model';
import { HistoryItem } from './models/history-item.model';
import { DifferentRangeResult } from './merging/models/different-range-result.model';
import MergingStrategyHelper from './merging/merging-strategy.helper';
import { MergeSession } from './merging/models/merge-session.model';
import { getDiffsInRange } from './adjuster-merger.helper';

export default class MergingHelper {
  private readonly _clientID: string;
  private readonly _markdownFile: MarkdownFile;

  private _toMergeModifications: LineModification[] = [];
  private _history: HistoryItem[] = [];

  private _session: MergeSession = {
    currentPosOfToMergeDiff: 0,
    toMergeIndex: 0,
    toMergeChanges: [],
  };

  public constructor(clientID: string, markdownFile: MarkdownFile) {
    this._clientID = clientID;
    this._markdownFile = markdownFile;
  }

  private static sumUpTwoLineContentModifications(
    lastChanges: LineContentModificationDTO,
    toSumUpChanges: LineContentModificationDTO,
  ): void {
    let currentOffsetLastDiff = 0;
    for (let i = 0; i < lastChanges.changes.length; ++i) {
      let lastDiff = lastChanges.changes[i];
      let currentOffsetToSumUp = 0;
      for (let j = 0; j < toSumUpChanges.changes.length; ++j) {
        const toSumUpDiff = toSumUpChanges.changes[j];
        if (Array.isArray(toSumUpDiff) && Array.isArray(lastDiff)) {
          const [toSumUpType, toSumUpItem] = toSumUpDiff;
          const [lastType, lastItemText] = lastDiff;
          if (
            currentOffsetToSumUp ===
              currentOffsetLastDiff + lastItemText.length &&
            toSumUpType === EChangeType.insertion &&
            lastType === EChangeType.insertion
          ) {
            lastDiff = [EChangeType.insertion, `${lastItemText}${toSumUpItem}`];
            lastChanges.changes.splice(i, 1, lastDiff);
            toSumUpChanges.changes[j] = toSumUpItem.length;
          }
        }

        currentOffsetToSumUp += MergingHelper.getOffsetIncrease(toSumUpDiff);
      }

      currentOffsetLastDiff += MergingHelper.getOffsetIncrease(lastDiff);
    }
  }

  private static getOffsetIncrease(item: TextDifferent): number {
    if (Array.isArray(item)) {
      const [lastDiffType, lastDiffItem] = item;
      if (lastDiffType === EChangeType.insertion) {
        return lastDiffItem.length;
      }
    } else {
      return item;
    }

    return 0;
  }

  public merge(
    history: HistoryItem[],
    toAdjustModifications: LineModification[],
  ): LineModification[] {
    this._toMergeModifications = toAdjustModifications;
    this._history = this.sumUpModificationHistoryCommitsOfCommitAuthor(history);

    for (const historyItem of this._history) {
      let historyLineIndex = 0;
      let toMergeLineIndex = 0;
      while (
        historyLineIndex < historyItem.changes.length &&
        toMergeLineIndex < this._toMergeModifications.length
      ) {
        const historyLine = historyItem.changes[historyLineIndex];
        const toMergeLine = this._toMergeModifications[toMergeLineIndex];

        if (historyLine.index === toMergeLine.index) {
          const isToMergeLineContentMod =
            toMergeLine instanceof LineContentModificationDTO;
          const isHistoryLineContentMod =
            historyLine instanceof LineContentModificationDTO;

          if (isToMergeLineContentMod && isHistoryLineContentMod) {
            const castedToMergeLine = (toMergeLine as LineContentModificationDTO).clone();
            const toMergeLineChangesCount = castedToMergeLine.changes.length;
            (this._toMergeModifications[
              toMergeLineIndex
            ] as LineContentModificationDTO).changes.splice(
              0,
              toMergeLineChangesCount,
              ...this.mergeSingleLine(
                castedToMergeLine.changes,
                (historyLine as LineContentModificationDTO).changes,
                historyItem.author,
              ),
            );

            ++historyLineIndex;
            ++toMergeLineIndex;
          } else if (isToMergeLineContentMod) {
            ++historyLineIndex;
          } else if (isHistoryLineContentMod) {
            ++toMergeLineIndex;
          }
        } else if (historyLine.index > toMergeLine.index) {
          ++historyLineIndex;
        } else {
          ++toMergeLineIndex;
        }
      }
    }

    return sumUpLineModifications(this._toMergeModifications);
  }

  private mergeSingleLine(
    toMergeChanges: TextDifferent[],
    historyChanges: TextDifferent[],
    historyAuthorID: string,
  ): TextDifferent[] {
    const historyDiffs = [...historyChanges]; // make flat copy to change this array

    this._session = {
      currentPosOfToMergeDiff: 0,
      toMergeIndex: 0,
      toMergeChanges: toMergeChanges,
    };

    for (
      this._session.toMergeIndex = 0;
      this._session.toMergeIndex < toMergeChanges.length;
      ++this._session.toMergeIndex
    ) {
      const toMergeDiff = toMergeChanges[this._session.toMergeIndex];
      // contains
      const endLen =
        this._session.currentPosOfToMergeDiff +
        (Array.isArray(toMergeDiff) ? toMergeDiff[1].length : toMergeDiff);

      // changes historyDiffs and  this._session.currentPosOfToMergeIndex
      this.adjustMergeSingleDiffToHistoryItemDiffs(
        historyDiffs,
        endLen,
        historyAuthorID,
      );
    }

    this._session.toMergeIndex = toMergeChanges.length - 1;
    ++this._session.currentPosOfToMergeDiff;
    this.adjustMergeSingleDiffToHistoryItemDiffs(
      historyDiffs,
      Number.MAX_VALUE,
      historyAuthorID,
    );

    return this._session.toMergeChanges;
  }

  private adjustMergeSingleDiffToHistoryItemDiffs(
    historyDiffs: TextDifferent[],
    endLen: number,
    authorID: string,
  ): void {
    const i = this._session.toMergeIndex;
    const toMergeDiff = this._session.toMergeChanges[i];
    // get diffs from historyItemDiffs which are inside the range of current pos (toMergeDiff position)
    const hDiffResults = getDiffsInRange(
      this._session.currentPosOfToMergeDiff,
      endLen,
      historyDiffs,
    );

    // check if "toMergeDiff" has changes (insertions and deletions)
    // AND if historyItem has changes in the current range
    if (Array.isArray(toMergeDiff) && hDiffResults.length > 0) {
      // okay we have a problem: two changes in the same range
      // we need to ride a merging strategy

      // changes historyDiffs, this._session.currentPosOfToMergeDiff, this._session.toMergeIndex
      this.handleToMergeInsertionOrDeletion(
        historyDiffs,
        hDiffResults,
        authorID,
      );
    } else if (!Array.isArray(toMergeDiff)) {
      this._session.currentPosOfToMergeDiff += toMergeDiff;
    }

    hDiffResults
      .sort((d1, d2) => d2.index - d1.index)
      .filter((d) => d.type === EChangeType.deletion && d.matchComplete)
      .forEach((d) => historyDiffs.splice(d.index, 1));
  }

  private handleToMergeInsertionOrDeletion(
    historyDiffs: TextDifferent[],
    hDiffResults: DifferentRangeResult[],
    authorID: string,
  ): void {
    let i = this._session.toMergeIndex;
    const toMergeDiff = this._session.toMergeChanges[i] as Diff; // checked by the caller
    const oldCurrentPosOfToMergeDiff = this._session.currentPosOfToMergeDiff;
    const mergingStrategist = new MergingStrategyHelper(
      this._clientID,
      toMergeDiff,
      historyDiffs,
    );

    const mergeStrategyResult = mergingStrategist.applyMergeStrategy(
      this._session.currentPosOfToMergeDiff,
      hDiffResults,
      authorID,
      i === this._session.toMergeChanges.length - 1,
    );
    this._session.currentPosOfToMergeDiff = mergeStrategyResult.newOffset;

    // the merging strategy has changed the offset, so adjust the "toMergeDiffs"
    if (oldCurrentPosOfToMergeDiff !== this._session.currentPosOfToMergeDiff) {
      const lastItem = i > 0 ? this._session.toMergeChanges[i - 1] : undefined;
      const offsetDifferent =
        this._session.currentPosOfToMergeDiff - oldCurrentPosOfToMergeDiff;
      // check if lastItem exists and if it is a normal number
      if (!!lastItem && !Array.isArray(lastItem)) {
        this._session.toMergeChanges[i - 1] =
          (lastItem as number) + offsetDifferent;
      } else {
        // lastItem doesn't exist OR it is not a number
        // => insert a normal number to adjust the offset

        if (offsetDifferent <= 0) throw new Error('Not implemented yet!');

        // contains the insert index
        const lastIndex = Math.max(0, i - 1); // if i > 0 then i - 1 else insert at 0
        this._session.toMergeChanges.splice(lastIndex, 0, offsetDifferent);
        ++i; // we have inserted something => adjust `i`!!
      }
    }
    /*
    else {
      const oldOffsetWithOldIncrease = MergerHelper.getOffsetIncrease(
        toMergeDiff,
      );
      let newOffsetIncreaseWithOldIncrease = 0;
      for (const newToMergeDiff of mergeStrategyResult.newToMergeDiff) {
        newOffsetIncreaseWithOldIncrease += MergerHelper.getOffsetIncrease(
          newToMergeDiff,
        );
      }

      if (newOffsetIncreaseWithOldIncrease < oldOffsetWithOldIncrease) {
        mergeStrategyResult.newToMergeDiff.push(
          oldOffsetWithOldIncrease - newOffsetIncreaseWithOldIncrease,
        );
      }
    }
    */

    this._session.toMergeChanges.splice(
      i,
      1,
      ...mergeStrategyResult.newToMergeDiff,
    );

    mergeStrategyResult.deleteIndices
      .sort((i1, i2) => i2 - i1)
      .forEach((index) => historyDiffs.splice(index, 1));

    this._session.toMergeIndex = i;
  }

  /**
   * Summarizes all commits from the user that the merge is currently referencing. "Missed" commits by oneself occur when the server did not respond fast enough.
   * These "missed" commits are summarized here.
   * @param historyItems
   * @private
   */
  private sumUpModificationHistoryCommitsOfCommitAuthor(
    historyItems: HistoryItem[],
  ): HistoryItem[] {
    const result: HistoryItem[] = [];

    for (const hItem of historyItems) {
      const lastItem = result[result.length - 1];
      // if lastItem exists and it is the author of the merge-request-user then...
      if (lastItem && lastItem.author === this._clientID) {
        // iterates over the changes of "lastItem"
        for (let iLast = 0; iLast < lastItem.changes.length; ++iLast) {
          const lastChanges = lastItem.changes[iLast]; // contains THE line insertion/deletion OR the changes of a line modification

          // iterates over the changes of the history item so that these changes are included in "lastItem" (if possible)
          for (let iToSumUp = 0; iToSumUp < hItem.changes.length; ++iToSumUp) {
            const toSumUpChanges = hItem.changes[iToSumUp];

            // if both items are line modifications and they are on the same line then...
            if (
              lastChanges instanceof LineContentModificationDTO &&
              toSumUpChanges instanceof LineContentModificationDTO &&
              lastChanges.index === toSumUpChanges.index
            ) {
              // changes (sum up) the content of "lastChanges.changes" and "toSumUpChanges"
              MergingHelper.sumUpTwoLineContentModifications(
                lastChanges,
                toSumUpChanges,
              );
              result[result.length - 1].changes[iLast] = lastChanges;

              const sumUppedChanges = sumUpTextDiffs(toSumUpChanges.changes);
              if (
                sumUppedChanges.length === 1 &&
                !Array.isArray(sumUppedChanges[0])
              ) {
                hItem.changes.splice(iToSumUp);
                --iToSumUp;
              }
            }
          }
        }
      }

      if (hItem.changes.length > 0) result.push(hItem);
    }

    return result;
  }
}
