import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsInt, Min } from 'class-validator';
import { default as DTOSuper } from '../../submodule-models/line-modification.abstract';
import { ELineModifications } from '../../submodule-models/line-modifications.enum';

export default abstract class LineModificationDTO extends DTOSuper {
  @ApiProperty({
    description: '',
  })
  @IsInt()
  @Min(0)
  public index!: number;

  @ApiProperty({
    type: ELineModifications,
    description: '...',
  })
  @IsEnum(ELineModifications)
  public abstract readonly type: ELineModifications;

  protected constructor(model: Partial<LineModificationDTO>) {
    super(model);
  }
}
