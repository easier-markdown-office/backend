import { ApiProperty } from '@nestjs/swagger';
import { IsInt, Min, ValidateNested } from 'class-validator';
import { default as UpdateTextDTOSuper } from '../../submodule-models/update-text.dto';
import LineModificationDTO from './line-modification.dto';

export default class UpdateTextDTO extends UpdateTextDTOSuper {
  @ApiProperty({
    description: '',
  })
  @IsInt()
  @Min(0)
  public basedOn!: number;

  @ApiProperty({
    type: LineModificationDTO,
    description: '...',
    isArray: true,
  })
  @ValidateNested() // TODO: doesn't work
  public changes!: LineModificationDTO[];

  public constructor(model: Partial<UpdateTextDTO>) {
    super(model);
  }
}
