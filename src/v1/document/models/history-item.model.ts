import LineModification from '../../submodule-models/line-modification.abstract';

/**
 * Represents a commit.
 */
export type HistoryItem = {
  changes: LineModification[];
  hash: number;
  author: string;
};
