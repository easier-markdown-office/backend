import { Socket } from 'socket.io';

export type EventArgs = {
  oldRooms: string[];
  newRoom: string;
  client: Socket;
};
