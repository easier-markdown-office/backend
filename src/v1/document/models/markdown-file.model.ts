import { HistoryItem } from './history-item.model';

/**
 * Represents a repository.
 */
export type MarkdownFile = {
  id: string;
  name: string;
  lines: string[];
  startHash: number;
  history: HistoryItem[];
  isClearing: boolean;
};
