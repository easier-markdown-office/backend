import { ApiProperty } from '@nestjs/swagger';
import { IsEmail } from 'class-validator';
import { default as UserEmailDTOSuper } from '../../submodule-models/user-email.dto';

/**
 * Represents the result dto model for a successful login.
 */
export default class UserEmailDTO extends UserEmailDTOSuper {
  /** The token of the user. */
  @ApiProperty({
    description: 'The email address.',
  })
  @IsEmail()
  public email!: string;

  public constructor(model: Partial<UserEmailDTO>) {
    super(model);
  }
}
