import { Diff } from 'diff-match-patch';
import LineModification from '../submodule-models/line-modification.abstract';
import { MarkdownFile } from './models/markdown-file.model';
import { HistoryItem } from './models/history-item.model';
import { LineContentModificationDTO } from '../submodule-models/line-content-modification.dto';
import { LineInsertionDTO } from '../submodule-models/line-insertion.dto';
import { LineDeletionDTO } from '../submodule-models/line-deletion.dto';
import { getDiffsInRange, sumUpHistoryItem } from './adjuster-merger.helper';
import { DifferentRangeResult } from './merging/models/different-range-result.model';
import { TextDifferent } from '../submodule-models/text-different.model';
import { EChangeType } from '../submodule-models/change-type.enum';
import { sumUpLineModifications } from '../submodule-models/utitlity.static';

export default class AdjusterHelper {
  private readonly _clientID: string;
  private readonly _markdownFile: MarkdownFile;

  private _toAdjustModifications: LineModification[] = [];
  private _history: HistoryItem[] = [];

  public constructor(clientID: string, markdownFile: MarkdownFile) {
    this._clientID = clientID;
    this._markdownFile = markdownFile;
  }

  private static handleToMergeEquality(
    hDiffResults: DifferentRangeResult[],
    historyDiffs: TextDifferent[],
    toAdjustChanges: TextDifferent[],
    toAdjustChangeIndex: number,
    currentToAdjustOffset: number,
  ): {
    changeOffsetBy: number;
    toAdjustChanges: TextDifferent[];
    applyIDs: number[];
  } {
    let spliceI = toAdjustChangeIndex;
    let tempOffset = currentToAdjustOffset;
    const oldLen = toAdjustChanges.length;
    const applyIDs: number[] = [];
    for (const hDiffResult of hDiffResults) {
      const hDiffItem = historyDiffs[hDiffResult.index] as [number, string];
      const [hDiffType, hDiffContent] = hDiffItem;
      if (hDiffType === EChangeType.insertion) {
        const toMergeEquality = toAdjustChanges[spliceI] as number;
        const lenBeforeInsertion = hDiffResult.offset - tempOffset;
        toAdjustChanges.splice(
          spliceI,
          1,
          lenBeforeInsertion, // new value of the element (toMergeDiffs at index "spliceI")
          hDiffContent.length, // add a "equality" with the length of insertion
          toMergeEquality - lenBeforeInsertion, // add the rest (different between lenBeforeInsertion and original length)
        );
        ++spliceI;

        tempOffset += hDiffItem[1].length;
        applyIDs.push(hDiffResult.index);
      } else if (
        hDiffType === EChangeType.deletion &&
        hDiffResult.matchComplete
      ) {
        toAdjustChanges[spliceI] =
          (toAdjustChanges[spliceI] as number) - hDiffContent.length;

        applyIDs.push(hDiffResult.index);
      }
    }

    let changeOffsetBy = 0;
    for (let j = 0; j <= toAdjustChanges.length - oldLen; ++j) {
      changeOffsetBy += toAdjustChanges[toAdjustChangeIndex + j] as number;
    }

    return { changeOffsetBy, toAdjustChanges, applyIDs };
  }

  /**
   * TODO: translate
   * Diese Funktion passt alle "toAdjustMods" zu den verpassten Commits an.
   * Es werden dabei Offsets und Zeilen-Indices verschoben. Alle Änderungen, die einen Merge-Konflict auslösen,
   * werden als "history" zurückgegeben. History beinhaltet keine Elemente mehr, die dieser Algorithmus berücksichtig hat.
   *
   * @param historyIndex
   * @param toAdjustModifications
   */
  public adjust(
    historyIndex: number,
    toAdjustModifications: LineModification[],
  ): { toAdjustModifications: LineModification[]; history: HistoryItem[] } {
    this._toAdjustModifications = toAdjustModifications;

    // make deep copy and then splice out the irrelevant part (0-historyIndex)
    // reminder: splice returns the array part that cut off
    this._history = [...this._markdownFile.history]
      .splice(historyIndex)
      .map((historyItem) => {
        const clone = JSON.parse(JSON.stringify(historyItem));
        clone.changes = historyItem.changes.map((mod) => mod.clone());

        return clone;
      });

    for (let i = 0; i < this._history.length; ++i) {
      const historyItem = this._history[i];
      this._history[i].changes = this.adjustModsToSingleHistoryItem(
        historyItem.changes,
        historyItem.author,
      );
    }

    return {
      toAdjustModifications: sumUpLineModifications(
        this._toAdjustModifications,
      ),
      history: sumUpHistoryItem(this._history),
    };
  }

  private adjustModsToSingleHistoryItem(
    historyChanges: LineModification[],
    historyAuthor: string,
  ): LineModification[] {
    for (const hMod of historyChanges) {
      if (hMod instanceof LineContentModificationDTO) {
        this.adjustLineOffset(hMod);
      } else if (
        hMod instanceof LineInsertionDTO ||
        hMod instanceof LineDeletionDTO
      ) {
        this.adjustLineIndices(hMod, historyAuthor);
      }
    }

    return historyChanges.filter(
      (m) => m instanceof LineContentModificationDTO,
    );
  }

  private adjustLineIndices(
    hMod: LineInsertionDTO | LineDeletionDTO,
    historyAuthor: string,
  ): void {
    const isInsertion = hMod instanceof LineInsertionDTO;
    for (let i = 0; i < this._toAdjustModifications.length; ++i) {
      const toAdjust = this._toAdjustModifications[i];
      if (isInsertion) {
        // ignore only own insertions!!
        // (im fall von "text einfügen" (strg + v) könnte sonst passieren, dass ein Haufen von Merge-Konflikte entstehen)
        if (toAdjust.index === hMod.index && historyAuthor === this._clientID) {
          this._toAdjustModifications.splice(i, 1);
          break;
        } else if (toAdjust.index >= hMod.index) {
          ++toAdjust.index;
        }
      } else {
        // ignore only own insertions!!
        // (im fall von "text einfügen" (strg + v) könnte sonst passieren, dass ein Haufen von Merge-Konflikte entstehen)
        if (toAdjust.index === hMod.index && historyAuthor === this._clientID) {
          this._toAdjustModifications.splice(i, 1);
          break;
        } else if (toAdjust.index >= hMod.index) {
          --toAdjust.index;
        }
      }
    }
  }

  private adjustLineOffset(hMod: LineContentModificationDTO): void {
    const hChangesCopy = [...hMod.changes];
    for (let i = 0; i < this._toAdjustModifications.length; ++i) {
      const toAdjust = this._toAdjustModifications[i];
      if (
        !(toAdjust instanceof LineContentModificationDTO) ||
        toAdjust.index !== hMod.index
      )
        continue;

      let currentLineOffset = 0;
      for (let j = 0; j < toAdjust.changes.length; ++j) {
        const toAdjustChange = toAdjust.changes[j];
        const rangeEnd =
          currentLineOffset +
          (Array.isArray(toAdjustChange)
            ? toAdjustChange[1].length
            : toAdjustChange);

        const { newJ, changeOffsetBy } = this.adjustSingleChangeOffset(
          toAdjust,
          hMod,
          hChangesCopy,
          currentLineOffset,
          rangeEnd,
          j,
        );
        j = newJ;
        currentLineOffset += changeOffsetBy;
      }

      ++currentLineOffset;
      this.adjustSingleChangeOffset(
        toAdjust,
        hMod,
        hChangesCopy,
        currentLineOffset,
        Number.MAX_VALUE,
        toAdjust.changes.length - 1,
      );
    }
  }

  private adjustSingleChangeOffset(
    toAdjust: LineContentModificationDTO,
    hMod: LineContentModificationDTO,
    hChangesCopy: TextDifferent[],
    currentLineOffset: number,
    rangeEnd: number,
    j: number,
  ): { changeOffsetBy: number; newJ: number } {
    let changeOffsetBy = 0;
    const toAdjustChange = toAdjust.changes[j];
    const hChangesInRange = getDiffsInRange(
      currentLineOffset,
      rangeEnd,
      hChangesCopy,
    );

    // if NOT insertion on insertion/deletion AND not deletion on insertion/deletion (because that is a case to merge)
    //
    if (!Array.isArray(toAdjustChange) && hChangesInRange.length > 0) {
      // toMergeDiff is not an insertion/deletion
      // so it is number of "equality"
      // adjust this number

      // changes this._session.currentPosOfToMergeDiff
      const changes = AdjusterHelper.handleToMergeEquality(
        hChangesInRange,
        hChangesCopy,
        toAdjust.changes,
        j,
        currentLineOffset,
      );

      for (const hChange of hChangesInRange
        .sort((hC1, hC2) => hC2.index - hC1.index)
        .filter(
          (hC) =>
            changes.applyIDs.find((index) => index === hC.index) !== undefined,
        )) {
        if (hChange.type === EChangeType.insertion) {
          const itemLen = (hMod.changes[hChange.index] as Diff)[1].length;
          hMod.changes[hChange.index] = itemLen;
          hChangesCopy[hChange.index] = itemLen;
        } else if (hChange.type === EChangeType.deletion) {
          hMod.changes.splice(hChange.index, 1);
          hChangesCopy.splice(hChange.index, 1);
        }
      }

      const oldAdjustChangesLength = toAdjust.changes.length;
      toAdjust.changes.splice(
        0,
        toAdjust.changes.length,
        ...changes.toAdjustChanges,
      );
      j -= toAdjust.changes.length - oldAdjustChangesLength;
      changeOffsetBy += changes.changeOffsetBy;
    } else if (Array.isArray(toAdjustChange)) {
      const [toAdjustChangeDiff, toAdjustChangeItem] = toAdjustChange;

      if (toAdjustChangeDiff === EChangeType.insertion) {
        changeOffsetBy += toAdjustChangeItem.length;
      }
    } else {
      changeOffsetBy += toAdjustChange;
    }

    hChangesInRange
      .filter((d) => d.type === EChangeType.deletion)
      .forEach((d) => (hChangesCopy[d.index] = 0));

    return { changeOffsetBy, newJ: j };
  }
}
