import { Test, TestingModule } from '@nestjs/testing';
import { Types } from 'mongoose';
import { DocumentWsService } from './document-ws.service';
import { HistoryItem } from './models/history-item.model';
import { LineInsertionDTO } from '../submodule-models/line-insertion.dto';
import { MarkdownFile } from './models/markdown-file.model';
import { LineDeletionDTO } from '../submodule-models/line-deletion.dto';
import { LineContentModificationDTO } from '../submodule-models/line-content-modification.dto';

describe('Document Service', () => {
  const client1 = 'Client 1';
  const client2 = 'Client 2';

  let documentID1 = '';
  let documentID2 = '';
  let documentIDCode = '';

  let service: DocumentWsService;

  beforeEach(async () => {
    (DocumentWsService.prototype as any).init = function (this: any) {
      this._markdownFiles.push({
        id: new Types.ObjectId().toHexString(),
        name: 'general',
        lines: ['Dies ist ein Text'],
        startHash: 0,
        history: [] as HistoryItem[],
        isClearing: false,
      } as MarkdownFile);

      documentID1 = this._markdownFiles[this._markdownFiles.length - 1].id;

      this._markdownFiles.push({
        id: new Types.ObjectId().toHexString(),
        name: 'lines',
        lines: [
          'Dies ist ein Text',
          'welcher in mehreren',
          'Zeilen aufgeteilt ist',
          'Dies ist die',
          'neue Generation!',
        ],
        startHash: 0,
        history: [] as HistoryItem[],
        isClearing: false,
      } as MarkdownFile);

      documentID2 = this._markdownFiles[this._markdownFiles.length - 1].id;

      this._markdownFiles.push({
        id: new Types.ObjectId().toHexString(),
        name: 'lines',
        lines: ['```', '', '```'],
        startHash: 0,
        history: [] as HistoryItem[],
        isClearing: false,
      } as MarkdownFile);
      documentIDCode = this._markdownFiles[this._markdownFiles.length - 1].id;
    };
    const module: TestingModule = await Test.createTestingModule({
      providers: [DocumentWsService],
    }).compile();

    service = module.get<DocumentWsService>(DocumentWsService);
    await service.onModuleInit();
  });

  afterEach(async () => {
    await service.onModuleDestroy();
  });

  describe('changes of two contributors on same commit', () => {
    it('should adjust the second line insertion to the first line insertion', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [new LineInsertionDTO({ index: 1 })],
        },
        client1,
        documentID2,
      );
      service.changeText(
        {
          basedOn: 0,
          changes: [new LineInsertionDTO({ index: 3 })],
        },
        client2,
        documentID2,
      );

      expect(service.getCompleteText(documentID2).lines).toMatchObject([
        'Dies ist ein Text',
        '',
        'welcher in mehreren',
        'Zeilen aufgeteilt ist',
        '',
        'Dies ist die',
        'neue Generation!',
      ]);
    });

    it('should adjust the second line insertion to the first line deletion', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [new LineDeletionDTO({ index: 1 })],
        },
        client1,
        documentID2,
      );
      service.changeText(
        {
          basedOn: 0,
          changes: [new LineInsertionDTO({ index: 3 })],
        },
        client2,
        documentID2,
      );

      expect(service.getCompleteText(documentID2).lines).toMatchObject([
        'Dies ist ein Text',
        'Zeilen aufgeteilt ist',
        '',
        'Dies ist die',
        'neue Generation!',
      ]);
    });

    it('should adjust the content of the second commit on the first commit (insertion)', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [5, [1, '... '], 12],
            }),
          ],
        },
        client1,
        documentID2,
      );
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [13, [1, 'cooler '], 4],
            }),
          ],
        },
        client2,
        documentID2,
      );

      expect(service.getCompleteText(documentID2).lines).toMatchObject([
        'Dies ... ist ein cooler Text',
        'welcher in mehreren',
        'Zeilen aufgeteilt ist',
        'Dies ist die',
        'neue Generation!',
      ]);
    });

    it('should adjust the content of the second commit on the first commit (insertion) (exceeded positions)', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [4, [1, 'er Unit-Test'], 12],
            }),
          ],
        },
        client1,
        documentID2,
      );
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [5, [1, 'soll... '], 11],
            }),
          ],
        },
        client2,
        documentID2,
      );

      expect(service.getCompleteText(documentID2).lines).toMatchObject([
        'Dieser Unit-Test soll... ist ein Text',
        'welcher in mehreren',
        'Zeilen aufgeteilt ist',
        'Dies ist die',
        'neue Generation!',
      ]);
    });

    it('should adjust the content of the second commit on the first commit (deletion)', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [5, [-1, 'ist '], 8],
            }),
          ],
        },
        client1,
        documentID2,
      );
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [13, [1, 'cooler '], 4],
            }),
          ],
        },
        client2,
        documentID2,
      );

      expect(service.getCompleteText(documentID2).lines).toMatchObject([
        'Dies ein cooler Text',
        'welcher in mehreren',
        'Zeilen aufgeteilt ist',
        'Dies ist die',
        'neue Generation!',
      ]);
    });

    it('should adjust two deletions at different positions', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [[-1, 'Dies '], 12],
            }),
          ],
        },
        client1,
        documentID1,
      );
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [5, [-1, 'ist ein '], 5],
            }),
          ],
        },
        client2,
        documentID1,
      );
      expect(service.getCompleteText(documentID1).lines).toMatchObject([
        'Text',
      ]);
    });
  });

  describe('changes of same contributor on same commit', () => {
    it('should adjust (ignore) the first line insertion in the second commit', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [new LineInsertionDTO({ index: 1 })],
        },
        client1,
        documentID2,
      );
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineInsertionDTO({ index: 1 }),
            new LineInsertionDTO({ index: 2 }),
          ],
        },
        client1,
        documentID2,
      );

      expect(service.getCompleteText(documentID2).lines).toMatchObject([
        'Dies ist ein Text',
        '',
        '',
        'welcher in mehreren',
        'Zeilen aufgeteilt ist',
        'Dies ist die',
        'neue Generation!',
      ]);
    });

    it('should insert two markdown code chars', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [17, [1, '`']],
            }),
          ],
        },
        client1,
        documentID1,
      );

      const endResult = service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [17, [1, '``']],
            }),
          ],
        },
        client1,
        documentID1,
      );
      expect(service.getCompleteText(documentID1).lines).toMatchObject([
        'Dies ist ein Text``',
      ]);
    });

    it('should "insert" test in three commits', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [0, [1, 't']],
            }),
          ],
        },
        client1,
        documentID1,
      );

      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [0, [1, 'te']],
            }),
          ],
        },
        client1,
        documentID1,
      );

      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [0, [1, 'test']],
            }),
          ],
        },
        client1,
        documentID1,
      );

      expect(service.getCompleteText(documentID1).lines).toMatchObject([
        'testDies ist ein Text',
      ]);
    });
  });

  describe('merging of two commits of the same person on the same parent commit', () => {
    it('should merge insertions on the same commit', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [13, [1, 'co']],
            }),
          ],
        },
        client1,
        documentID2,
      );
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [13, [1, 'cooler']],
            }),
          ],
        },
        client1,
        documentID2,
      );
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [13, [1, 'cooler ']],
            }),
          ],
        },
        client1,
        documentID2,
      );
      expect(service.getCompleteText(documentID2).lines).toMatchObject([
        'Dies ist ein cooler Text',
        'welcher in mehreren',
        'Zeilen aufgeteilt ist',
        'Dies ist die',
        'neue Generation!',
      ]);
    });

    it('should do a special merge (same author, two commits with same parent commit)', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [[1, 'E'], 17],
            }),
          ],
        },
        client1,
        documentID1,
      );

      const commitDE = service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [[1, 'DE'], 17],
            }),
          ],
        },
        client1,
        documentID1,
      );

      console.log(service.getCompleteText(documentID1).lines);
      expect(commitDE.clientResponse.missedMods).toMatchObject([
        [
          new LineContentModificationDTO({
            index: 0,
            changes: [[-1, 'DE'], 17],
          }),
        ],
        [new LineContentModificationDTO({ index: 0, changes: [[1, 'E'], 17] })],
        [
          new LineContentModificationDTO({
            index: 0,
            changes: [[1, 'D'], 18 - 1], // FIXME: 18 is right, 17 is wrong
          }),
        ],
      ]);
      expect(service.getCompleteText(documentID1).lines).toEqual([
        'DEDies ist ein Text',
      ]);
    });

    it('should insert a "dolor"', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [4, [1, ' '], 13],
            }),
          ],
        },
        client1,
        documentID1,
      );

      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [4, [1, ' d'], 13],
            }),
          ],
        },
        client1,
        documentID1,
      );

      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [4, [1, ' do'], 13],
            }),
          ],
        },
        client1,
        documentID1,
      );

      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [4, [1, ' dol'], 13],
            }),
          ],
        },
        client1,
        documentID1,
      );

      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [4, [1, ' dolo'], 13],
            }),
          ],
        },
        client1,
        documentID1,
      );

      const lastCommit = service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [4, [1, ' dolor'], 13],
            }),
          ],
        },
        client1,
        documentID1,
      );

      console.log(service.getCompleteText(documentID1).lines);
      /*
          expect(commitDE.clientResponse.missedMods).toMatchObject([
              [
                  new LineContentModificationDTO({
                      index: 0,
                      changes: [4, [1, ' d'], 13],
                  }),
              ],
              [new LineContentModificationDTO({ index: 0, changes: [[1, 'E'], 17] })],
              [
                  new LineContentModificationDTO({
                      index: 0,
                      changes: [[1, 'D'], 18 - 1], // FIXME: 18 is right, 17 is wrong
                  }),
              ],
          ]);

           */
      expect(service.getCompleteText(documentID1).lines).toEqual([
        'Dies dolor ist ein Text',
      ]);
    });
  });

  describe('merging of two commits of different person on the same parent commit', () => {
    it('should NOT merge two insertions at the same position', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [13, [1, 'cooler '], 4],
            }),
          ],
        },
        client1,
        documentID2,
      );

      expect(() =>
        service.changeText(
          {
            basedOn: 0,
            changes: [
              new LineContentModificationDTO({
                index: 0,
                changes: [13, [1, 'blöder '], 4],
              }),
            ],
          },
          client2,
          documentID2,
        ),
      ).toThrow(Error);
    });

    it('should merge three deletions at same position', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [[-1, 'Di'], [-1, 'es'], 12], // should not be happen as input, but check the nice alg!
            }),
          ],
        },
        client1,
        documentID1,
      );
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [[-1, 'Dies '], 12],
            }),
          ],
        },
        client2,
        documentID1,
      );
      expect(service.getCompleteText(documentID1).lines).toMatchObject([
        'ist ein Text',
      ]);
    });

    it('should merge three deletions at same position #2', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [[-1, 'Di'], [-1, 'es'], 12], // should not be happen as input, but check the nice alg!
            }),
          ],
        },
        client1,
        documentID1,
      );
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [[-1, 'Dies ist'], 9],
            }),
          ],
        },
        client2,
        documentID1,
      );
      expect(service.getCompleteText(documentID1).lines).toMatchObject([
        ' ein Text',
      ]);
    });

    it('should merge two deletions at an exceeded position', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [2, [-1, 'es '], 12],
            }),
          ],
        },
        client1,
        documentID1,
      );
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [[-1, 'Die'], 14],
            }),
          ],
        },
        client2,
        documentID1,
      );
      expect(service.getCompleteText(documentID1).lines).toMatchObject([
        'ist ein Text',
      ]);
    });

    it('should merge two deletions at an exceeded position #2', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [[-1, 'Die'], 14],
            }),
          ],
        },
        client1,
        documentID1,
      );
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [2, [-1, 'es'], 13],
            }),
          ],
        },
        client2,
        documentID1,
      );
      expect(service.getCompleteText(documentID1).lines).toMatchObject([
        ' ist ein Text',
      ]);
    });

    it('should merge two deletions at the same position', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [[-1, 'Dies '], 12],
            }),
          ],
        },
        client1,
        documentID1,
      );
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [[-1, 'D'], 16],
            }),
          ],
        },
        client2,
        documentID1,
      );
      expect(service.getCompleteText(documentID1).lines).toMatchObject([
        'ist ein Text',
      ]);
    });

    it('should merge two insertions at the same position if at the beginning of a document', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [[1, 'Beispiel: '], 17],
            }),
          ],
        },
        client1,
        documentID1,
      );
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [[1, 'In Markdown: '], 17],
            }),
          ],
        },
        client2,
        documentID1,
      );
      let before = false;
      let after = false;
      try {
        expect(service.getCompleteText(documentID1).lines).toMatchObject([
          'Beispiel: In Markdown: Dies ist ein Text',
        ]);
        before = true;
      } catch (e) {}
      try {
        expect(service.getCompleteText(documentID1).lines).toMatchObject([
          'In Markdown: Beispiel: Dies ist ein Text',
        ]);
        after = true;
      } catch (e) {}

      expect(before || after).toBeTruthy();
    });

    it('should merge two insertions at the same position if at the end of a document', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [17, [1, ' - das war toll']],
            }),
          ],
        },
        client1,
        documentID1,
      );
      service.changeText(
        {
          basedOn: 0,

          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [17, [1, ' (und in Markdown!)']],
            }),
          ],
        },
        client2,
        documentID1,
      );
      expect(service.getCompleteText(documentID1).lines).toMatchObject([
        'Dies ist ein Text - das war toll (und in Markdown!)',
      ]);
    });

    it('should merge multiple insertions', () => {
      const commitHash1 = service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [[1, 'E'], 17],
            }),
          ],
        },
        client1,
        documentID1,
      );
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [17, [1, 'F']],
            }),
          ],
        },
        client2,
        documentID1,
      );
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [17, [1, 'FG']],
            }),
          ],
        },
        client2,
        documentID1,
      );
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [17, [1, 'FGH']],
            }),
          ],
        },
        client2,
        documentID1,
      );

      const commitE = service.changeText(
        {
          basedOn: commitHash1.clientResponse.hash,
          changes: [
            new LineContentModificationDTO({
              index: 0,
              changes: [[1, 'D'], 17],
            }),
          ],
        },
        client1,
        documentID1,
      );
      commitE.clientResponse.missedMods.forEach((missedMod) =>
        console.log(missedMod),
      );
      expect(commitE.clientResponse.missedMods).toMatchObject([
        [
          new LineContentModificationDTO({
            index: 0,
            changes: [[-1, 'D'], 17],
          }),
        ],
        [new LineContentModificationDTO({ index: 0, changes: [18, [1, 'F']] })],
        [new LineContentModificationDTO({ index: 0, changes: [19, [1, 'G']] })],
        [new LineContentModificationDTO({ index: 0, changes: [20, [1, 'H']] })],
        [
          new LineContentModificationDTO({
            index: 0,
            changes: [[1, 'D'], 20 - 1],
          }),
        ], // FIXME: 20 is right, 19 is wrong
      ]);
      expect(service.getCompleteText(documentID1).lines).toEqual([
        'DEDies ist ein TextFGH',
      ]);
    });
  });
  /*
  describe('insertion + insertion (on the same text reference)', () => {



 */
  describe('fails in production...', () => {
    it('should insert a code into the file', () => {
      service.changeText(
        {
          basedOn: 0,
          changes: [
            new LineInsertionDTO({ index: 1 }),
            new LineInsertionDTO({ index: 2 }),
            new LineInsertionDTO({ index: 3 }),
            new LineInsertionDTO({ index: 4 }),
            new LineInsertionDTO({ index: 5 }),
            new LineInsertionDTO({ index: 6 }),
            new LineInsertionDTO({ index: 7 }),
            new LineInsertionDTO({ index: 8 }),
            new LineInsertionDTO({ index: 9 }),
            new LineInsertionDTO({ index: 10 }),
            new LineInsertionDTO({ index: 11 }),
            new LineInsertionDTO({ index: 12 }),
            new LineInsertionDTO({ index: 13 }),
            new LineInsertionDTO({ index: 14 }),
            new LineInsertionDTO({ index: 15 }),
            new LineInsertionDTO({ index: 16 }),
            new LineContentModificationDTO({
              index: 1,
              changes: [[1, 'import {']],
            }),
            new LineContentModificationDTO({
              index: 2,
              changes: [[1, '  Injectable,']],
            }),
            new LineContentModificationDTO({
              index: 3,
              changes: [[1, '  NestMiddleware,']],
            }),
            new LineContentModificationDTO({
              index: 4,
              changes: [[1, '  UnauthorizedException,']],
            }),
            new LineContentModificationDTO({
              index: 5,
              changes: [[1, "} from '@nestjs/common';"]],
            }),
            new LineContentModificationDTO({
              index: 6,
              changes: [[1, "import { Request, Response } from 'express';"]],
            }),
            new LineContentModificationDTO({
              index: 7,
              changes: [[1, "import { verify } from 'jsonwebtoken';"]],
            }),
            new LineContentModificationDTO({
              index: 8,
              changes: [[1, "import { Types } from 'mongoose';"]],
            }),
            new LineContentModificationDTO({
              index: 10,
              changes: [
                [1, "import { jwtPW } from '../../../config/config.json';"],
              ],
            }),
            new LineContentModificationDTO({
              index: 11,
              changes: [
                [
                  1,
                  "import BaseUserDBM from '../models/schemas/baseuser.schema';",
                ],
              ],
            }),
            new LineContentModificationDTO({
              index: 13,
              changes: [[1, '/**']],
            }),
            new LineContentModificationDTO({
              index: 14,
              changes: [
                [
                  1,
                  ' * This is a middleware that is supposed to authenticate a baseUser based on the token',
                ],
              ],
            }),
            new LineContentModificationDTO({
              index: 15,
              changes: [
                [
                  1,
                  ' * he sends to the server via the Authentication "Bearer" Header. In Return it sets the verified',
                ],
              ],
            }),
            new LineContentModificationDTO({
              index: 16,
              changes: [
                [
                  1,
                  ' * info from the database, and fills in the rest from the database.',
                ],
              ],
            }),
            new LineContentModificationDTO({
              index: 17,
              changes: [[1, ' */']],
            }),
          ],
        },
        client1,
        documentIDCode,
      );

      console.log(service.getCompleteText(documentIDCode).lines);
      expect(service.getCompleteText(documentIDCode).lines).toEqual([
        '```',
        'import {',
        '  Injectable,',
        '  NestMiddleware,',
        '  UnauthorizedException,',
        "} from '@nestjs/common';",
        "import { Request, Response } from 'express';",
        "import { verify } from 'jsonwebtoken';",
        "import { Types } from 'mongoose';",
        '',
        "import { jwtPW } from '../../../config/config.json';",
        "import BaseUserDBM from '../models/schemas/baseuser.schema';",
        '',
        '/**',
        ' * This is a middleware that is supposed to authenticate a baseUser based on the token',
        ' * he sends to the server via the Authentication "Bearer" Header. In Return it sets the verified',
        ' * info from the database, and fills in the rest from the database.',
        ' */',
        '```',
      ]);
    });
  });
});
