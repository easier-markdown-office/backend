import { inspect } from 'util';
import {
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { Types } from 'mongoose';
import { CompleteTextDTO } from '../submodule-models/complete-text.dto';
import { DocumentWsService } from './document-ws.service';
import { AdjustClientTextDTO } from '../submodule-models/adjust-client-text.dto';
import UtilityService from '../utility.service';
import { UseFilters, UseGuards, UsePipes } from '@nestjs/common';
import {
  DOCUMENT_EVENTS,
  DOCUMENT_MESSAGES,
} from '../submodule-models/gateway-addresses.constants';
import { ERoom } from '../models/room.enum';
import { WsTransformerFilter } from '../filters/ws-transformer.filter';
import { GlobalUserService } from '../global-user/global-user.service';
import { DocumentBridgeService } from './document-bridge.service';
import { WsAuthGuard } from '../guards/ws-auth.guard';
import { WsDocumentOwnerGuard } from '../guards/ws-document-owner.guard';
import DocumentDTO from '../models/document.dto';
import ParseObjectIdPipe from '../pipes/parse-object-id.pipe';
import { WsWrapperPipe } from '../pipes/ws-wrapper.pipe';
import ValidationPipe from '../pipes/validation.pipe';
import UpdateTextDTO from './models/update-text.dto';
import { DocumentNamePipe } from '../pipes/document-name.pipe';
import { WsEmailExistsGuard } from '../guards/ws-email-exists.guard';
import DocumentDocument from '../models/documents/document.document';
import UserDocument from '../models/documents/user.document';
import { WsDocumentContributorGuard } from '../guards/ws-document-contributor.guard';
import { MergingExceptionFilter } from '../filters/merging-exception.filter';

@WebSocketGateway()
@UseFilters(WsTransformerFilter)
export class DocumentGateway implements OnGatewayInit<Server> {
  @WebSocketServer()
  public server: Server | undefined;

  public constructor(
    private readonly service: DocumentWsService,
    private readonly globalUserService: GlobalUserService,
    private readonly bridgeService: DocumentBridgeService,
  ) {
    this.globalUserService.newConnection.subscribe(() => {
      console.log('new connection!');
    });

    this.globalUserService.toLoadDocuments.subscribe((files) => {
      files.forEach((f) => this.service.loadMarkdown(f.id, f.name, f.lines));
    });
  }

  @UseGuards(WsAuthGuard)
  @UsePipes(new WsWrapperPipe(new ParseObjectIdPipe()))
  @SubscribeMessage(DOCUMENT_MESSAGES.completeText)
  public async handleDocumentRequest(
    client: Socket,
    mongoID: Types.ObjectId,
  ): Promise<CompleteTextDTO | void> {
    const id = mongoID.toHexString();
    if (!UtilityService.isInRoom(client, `${ERoom.currentDocument}${id}`)) {
      this.changeSelectedDocument(client, id);
    }

    return this.service.getCompleteText(id);
  }

  @UseGuards(WsAuthGuard)
  @UsePipes(new WsWrapperPipe(new ParseObjectIdPipe()))
  @SubscribeMessage(DOCUMENT_MESSAGES.completeTextWithoutSideEffects)
  public async handleDocumentRequestWithoutRoomChange(
    client: Socket,
    mongoID: Types.ObjectId,
  ): Promise<CompleteTextDTO | void> {
    const id = mongoID.toHexString();
    return this.service.getCompleteText(id);
  }

  @UsePipes(new WsWrapperPipe(new ValidationPipe<UpdateTextDTO>(UpdateTextDTO)))
  @SubscribeMessage(DOCUMENT_MESSAGES.changeText)
  @UseFilters(MergingExceptionFilter)
  public handleDocumentModification(
    client: Socket,
    payload: UpdateTextDTO,
  ): AdjustClientTextDTO {
    // await new Promise((resolve) => setTimeout(resolve, 1000 * 2.0));
    const docID = UtilityService.getMongoObjectIDFromRoom(
      client,
      ERoom.currentDocument,
    );
    if (!docID) throw new Error('Unexpected behaviour.');

    console.log(
      '-- start --',
      client.id,
      inspect(payload.changes, false, null, true),
    );
    const result = this.service.changeText(payload, client.id, docID);

    client
      .to(`${ERoom.currentDocument}${docID}`)
      .broadcast.emit(DOCUMENT_EVENTS.textChanged, result.broadcast);
    console.log('-- end --');
    return result.clientResponse;
  }

  @UsePipes(new WsWrapperPipe(new DocumentNamePipe()))
  @SubscribeMessage(DOCUMENT_MESSAGES.create)
  public async handleDocumentCreation(
    client: Socket,
    docName: string,
  ): Promise<string> {
    // await new Promise((resolve) => setTimeout(resolve, 1000 * 5.0));
    const userID = UtilityService.getMongoObjectIDFromRoom(client, ERoom.user);
    if (!userID) throw new Error('Bad Request');

    return await this.bridgeService.createNewDocumentAndEmitEvents(
      docName,
      userID,
    );
  }

  @UseGuards(WsDocumentOwnerGuard)
  @UsePipes(new WsWrapperPipe(new ParseObjectIdPipe()))
  @SubscribeMessage(DOCUMENT_MESSAGES.delete)
  public async handleDocumentDeletion(
    client: Socket,
    docID: Types.ObjectId,
  ): Promise<boolean> {
    // await new Promise((resolve) => setTimeout(resolve, 1000 * 5.0));
    const userID = UtilityService.getMongoObjectIDFromRoom(
      client,
      ERoom.user,
    ) as string;

    await this.bridgeService.deleteDocumentAndEmitEvents(
      docID.toHexString(),
      userID,
    );
    return true;
  }

  @UseGuards(WsDocumentOwnerGuard)
  @UsePipes(new WsWrapperPipe(new ValidationPipe<DocumentDTO>(DocumentDTO)))
  @SubscribeMessage(DOCUMENT_MESSAGES.rename)
  public async handleDocumentRename(
    client: Socket,
    doc: DocumentDTO,
  ): Promise<boolean> {
    const userID = UtilityService.getMongoObjectIDFromRoom(
      client,
      ERoom.user,
    ) as string;

    await this.bridgeService.renameDocumentAndEmitEvents(
      doc.id,
      userID,
      doc.name,
    );
    return true;
  }

  @UseGuards(WsDocumentOwnerGuard, WsEmailExistsGuard)
  @SubscribeMessage(DOCUMENT_MESSAGES.invite)
  public async handleInvite(client: Socket): Promise<boolean> {
    await this.bridgeService.inviteAndEmitEvents(
      client.selectedDocument as DocumentDocument, // checked by guard
      client.checkedEmailAddressUser as UserDocument, // checked by guard
      client.user as UserDocument, // checked by guard
    );
    return true;
  }

  @UseGuards(WsDocumentOwnerGuard, WsEmailExistsGuard)
  @SubscribeMessage(DOCUMENT_MESSAGES.discharge)
  public async handleDischarge(client: Socket): Promise<boolean> {
    console.log('discharge called');
    await this.bridgeService.dischargeAndEmitEvents(
      client.selectedDocument as DocumentDocument, // checked by guard
      client.checkedEmailAddressUser as UserDocument, // checked by guard
      client.user as UserDocument, // checked by guard
    );
    return true;
  }

  @UseGuards(WsDocumentContributorGuard)
  @SubscribeMessage(DOCUMENT_MESSAGES.leave)
  public async handleLeave(client: Socket): Promise<boolean> {
    await this.bridgeService.leaveAndEmitEvents(
      client.selectedDocument as DocumentDocument, // checked by guard
      client.user as UserDocument, // checked by guard
    );
    return true;
  }

  public afterInit(server: Server) {
    this.bridgeService.wsServer = server;
  }

  private changeSelectedDocument(client: Socket, docID: string) {
    const newRoom = `${ERoom.currentDocument}${docID}`;
    const oldRooms: string[] = [];
    let selectedDocID;
    while (
      (selectedDocID = UtilityService.getMongoObjectIDFromRoom(
        client,
        ERoom.currentDocument,
      )) !== null
    ) {
      const socketRoomName = `${ERoom.currentDocument}${selectedDocID}`;
      delete client.rooms[socketRoomName];
      oldRooms.push(socketRoomName);
      client.leave(socketRoomName);
    }

    client.join(newRoom);
    this.globalUserService.roomChanged.next({ newRoom, oldRooms, client });
  }
}
