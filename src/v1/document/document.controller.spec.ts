import { Test, TestingModule } from '@nestjs/testing';
import { DocumentController } from './document.controller';
import { DocumentBridgeService } from './document-bridge.service';
import { DocumentHttpService } from './document-http.service';
import { DocumentWsService } from './document-ws.service';
import { DocumentGateway } from './document.gateway';
import { GlobalUserModule } from '../global-user/global-user.module';

describe('DocumentController', () => {
  let controller: DocumentController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DocumentHttpService,
        DocumentBridgeService,
        DocumentWsService,
        DocumentGateway,
      ],
      controllers: [DocumentController],
      imports: [GlobalUserModule],
    }).compile();

    controller = module.get<DocumentController>(DocumentController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
