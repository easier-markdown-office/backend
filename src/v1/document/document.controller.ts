import { Request } from 'express';
import {
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiParam,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { DocumentBridgeService } from './document-bridge.service';
import ParseObjectIdPipe from '../pipes/parse-object-id.pipe';
import { DocumentOwnerGuard } from '../guards/document-owner.guard';
import { AuthGuard } from '../guards/auth.guard';
import { EmailExistsGuard } from '../guards/email-exists.guard';
import UserEmailDTO from './models/user-email.dto';
import DocumentDocument from '../models/documents/document.document';
import UserDocument from '../models/documents/user.document';
import { DocumentContributorGuard } from '../guards/document-contributor.guard';
import { DocumentNamePipe } from '../pipes/document-name.pipe';
import ContributorInfoDTO from '../submodule-models/contributor-info.dto';

@ApiBearerAuth()
@UseGuards(AuthGuard)
@Controller('document')
@ApiTags('document')
export class DocumentController {
  public constructor(private readonly bridgeService: DocumentBridgeService) {}

  @Post('create/:docName')
  public async createDocument(
    @Req() req: Request,
    @Param('docName', DocumentNamePipe) docName: string,
  ): Promise<string> {
    return await this.bridgeService.createNewDocumentAndEmitEvents(
      docName,
      req.user?.id.toString(),
    );
  }

  @UseGuards(DocumentOwnerGuard)
  @Delete(':docID/remove')
  @ApiParam({
    name: 'docID',
  })
  public async removeDocument(
    @Req() req: Request,
    @Param('docID', ParseObjectIdPipe) docID: string,
  ) {
    return await this.bridgeService.deleteDocumentAndEmitEvents(
      docID,
      req.user?.id.toString(),
    );
  }

  @UseGuards(DocumentOwnerGuard)
  @Put(':docID/rename/:newName')
  @ApiParam({
    name: 'docID',
  })
  @ApiParam({
    name: 'newName',
  })
  public async renameDocument(
    @Req() req: Request,
    @Param('docID', ParseObjectIdPipe) docID: string,
    @Param('newName', DocumentNamePipe) newName: string,
  ) {
    return await this.bridgeService.renameDocumentAndEmitEvents(
      docID,
      req.user?.id.toString(),
      newName,
    );
  }

  @UseGuards(DocumentOwnerGuard, EmailExistsGuard)
  @Put(':docID/invite')
  @ApiParam({
    name: 'docID', // will use in DocumentGuard
  })
  @ApiBody({
    type: UserEmailDTO,
  })
  @ApiResponse({
    type: ContributorInfoDTO,
    status: HttpStatus.OK,
  })
  public async invite(@Req() req: Request): Promise<ContributorInfoDTO> {
    await this.bridgeService.inviteAndEmitEvents(
      req.selectedDocument as DocumentDocument, // checked by the guard
      req.checkedEmailAddressUser as UserDocument, // checked by the guard
      req.user as UserDocument, // checked by the guard
    );

    return new ContributorInfoDTO({
      username: req.checkedEmailAddressUser?.username,
      email: req.checkedEmailAddressUser?.email,
    });
  }

  @UseGuards(DocumentOwnerGuard, EmailExistsGuard)
  @Put(':docID/discharge')
  @ApiParam({
    name: 'docID', // will use in DocumentGuard
  })
  @ApiBody({
    type: UserEmailDTO,
  })
  public async discharge(@Req() req: Request) {
    await this.bridgeService.dischargeAndEmitEvents(
      req.selectedDocument as DocumentDocument, // checked by the guard
      req.checkedEmailAddressUser as UserDocument, // checked by the guard
      req.user as UserDocument, // checked by the guard
    );
  }

  @UseGuards(DocumentContributorGuard)
  @Put(':docID/leave')
  @ApiParam({
    name: 'docID', // will use in DocumentGuard
  })
  public async leave(@Req() req: Request) {
    await this.bridgeService.leaveAndEmitEvents(
      req.selectedDocument as DocumentDocument, // checked by the guard
      req.user as UserDocument, // checked by the guard
    );
  }

  @UseGuards(DocumentOwnerGuard)
  @Get(':docID/contributors')
  @ApiParam({
    name: 'docID', // will use in DocumentGuard
  })
  @ApiResponse({
    type: ContributorInfoDTO,
    status: HttpStatus.OK,
    isArray: true,
  })
  public async contributors(
    @Req() req: Request,
  ): Promise<ContributorInfoDTO[]> {
    return await this.bridgeService.getContributors(
      req.selectedDocument as DocumentDocument, // checked by the guard
    );
  }
}
