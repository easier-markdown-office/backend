import { ApiProperty } from '@nestjs/swagger';
import { IsInt, Min } from 'class-validator';
import { default as DTOSuper } from '../../submodule-models/update-cursor.dto';

export default class UpdateCursorDTO extends DTOSuper {
  @ApiProperty({
    description: '',
  })
  @IsInt()
  @Min(-1)
  public start!: number;

  @ApiProperty({
    description: '',
  })
  @IsInt()
  @Min(-1)
  public end!: number;

  public constructor(model: Partial<UpdateCursorDTO>) {
    super(model);
  }
}
