import { Test, TestingModule } from '@nestjs/testing';
import { CursorService } from './cursor.service';
import { GlobalUserModule } from '../global-user/global-user.module';

describe('CursorService', () => {
  let service: CursorService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CursorService],
      imports: [GlobalUserModule],
    }).compile();

    service = module.get<CursorService>(CursorService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
