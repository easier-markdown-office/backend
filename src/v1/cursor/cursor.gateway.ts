import {
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { CursorService } from './cursor.service';
import { CursorContributorDTO } from '../submodule-models/cursor-contributor.dto';
import UtilityService from '../utility.service';
import { EventArgs } from '../document/models/event-args.model';
import {
  CURSOR_EVENTS,
  CURSOR_MESSAGES,
} from '../submodule-models/gateway-addresses.constants';
import { ERoom } from '../models/room.enum';
import { GlobalUserService } from '../global-user/global-user.service';
import { UsePipes } from '@nestjs/common';
import { WsWrapperPipe } from '../pipes/ws-wrapper.pipe';
import ValidationPipe from '../pipes/validation.pipe';
import UpdateCursorDTO from './models/update-cursor.dto';

@WebSocketGateway()
export class CursorGateway implements OnGatewayDisconnect<Socket> {
  @WebSocketServer()
  private readonly _server: Server | undefined;

  public constructor(
    private readonly service: CursorService,
    private readonly globalUserService: GlobalUserService,
  ) {
    this.globalUserService.roomChanged.subscribe(this.roomChanged.bind(this));
  }

  @UsePipes(
    new WsWrapperPipe(new ValidationPipe<UpdateCursorDTO>(UpdateCursorDTO)),
  )
  @SubscribeMessage(CURSOR_MESSAGES.change)
  public handleCursorChanging(client: Socket, payload: UpdateCursorDTO): void {
    const docID = UtilityService.getMongoObjectIDFromRoom(
      client,
      ERoom.currentDocument,
    );
    if (!docID) return;
    const room = `${ERoom.currentDocument}${docID}`;

    this.service.changeCursor(room, client.id, payload);
    client.to(room).broadcast.emit(CURSOR_EVENTS.changed, {
      ...payload,
      id: client.id,
    } as CursorContributorDTO);
  }

  public handleDisconnect(client: Socket) {
    this.service.removeUserCompletely(client.id);
  }

  private roomChanged(obj: EventArgs): void {
    obj.oldRooms.forEach((r) => {
      this.service.remove(r, obj.client.id);
      this._server?.to(r).emit(CURSOR_EVENTS.contriRemoved, obj.client.id);
    });
    this.service.add(obj.newRoom, obj.client.id);
    obj.client
      .to(obj.newRoom)
      .broadcast.emit(CURSOR_EVENTS.contriAdded, obj.client.id);
    obj.client.emit(
      CURSOR_EVENTS.cursors,
      this.service.getAllCursors(obj.newRoom),
    );
  }
}
