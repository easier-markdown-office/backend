import { Injectable } from '@nestjs/common';
import { CursorContributorDTO } from '../submodule-models/cursor-contributor.dto';
import UpdateCursorDTO from './models/update-cursor.dto';

@Injectable()
export class CursorService {
  private readonly _cursors: { [key: string]: CursorContributorDTO[] } = {};

  public getAllCursors(roomName: string): CursorContributorDTO[] {
    if (this._cursors[roomName]) return [...this._cursors[roomName]]; // make flat copy, so it is not possible to change this value outside

    return [];
  }

  public changeCursor(roomName: string, id: string, cursor: UpdateCursorDTO) {
    const cursorIndex = this.getCursorIndexFromID(roomName, id);
    if (cursorIndex < 0) {
      if (!this._cursors[roomName]) this._cursors[roomName] = [];

      this._cursors[roomName].push({ ...cursor, id });
      return;
    }

    this._cursors[roomName][cursorIndex].start = cursor.start;
    this._cursors[roomName][cursorIndex].end = cursor.end;
  }

  public add(roomName: string, id: string): void {
    const cursorIndex = this.getCursorIndexFromID(roomName, id);
    if (cursorIndex >= 0) return;

    if (!this._cursors[roomName]) this._cursors[roomName] = [];

    this._cursors[roomName].push({ id, start: 0, end: 0 });
  }

  public remove(roomName: string, id: string) {
    const cursorIndex = this.getCursorIndexFromID(roomName, id);
    if (cursorIndex >= 0) {
      this._cursors[roomName].splice(cursorIndex, 1);
    }
  }

  public removeUserCompletely(id: string) {
    for (const roomName of Object.keys(this._cursors)) {
      const userIndex = this._cursors[roomName].findIndex((cc) => cc.id === id);
      if (userIndex >= 0) this._cursors[roomName].splice(userIndex, 1);
    }
  }

  private getCursorIndexFromID(roomName: string, id: string): number {
    const roomCursors = this._cursors[roomName];
    if (!roomCursors) return -1;

    return roomCursors.findIndex((cc) => cc.id == id);
  }
}
