import { Module } from '@nestjs/common';
import { CursorGateway } from './cursor.gateway';
import { CursorService } from './cursor.service';
import { GlobalUserModule } from '../global-user/global-user.module';

@Module({
  providers: [CursorGateway, CursorService],
  imports: [GlobalUserModule],
})
export class CursorModule {}
