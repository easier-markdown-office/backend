import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString, Min } from 'class-validator';
import { default as DocumentDTOSuper } from '../submodule-models/document.dto';
import { IsMongoDBObjectID } from '../decorators/is-mongodb-object-id.decorator';

export default class DocumentDTO extends DocumentDTOSuper {
  @ApiProperty({
    description: '...',
  })
  @IsString()
  @IsMongoDBObjectID()
  public id!: string;

  @ApiProperty({
    description: '...',
  })
  @IsString()
  public name!: string;

  @ApiProperty({
    description: '...',
  })
  @IsInt()
  @Min(0)
  public modDate!: number;

  public constructor(model: Partial<DocumentDTO>) {
    super(model);
  }
}
