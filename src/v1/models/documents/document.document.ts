import { Document } from 'mongoose';
import UserDocument from './user.document';

/**
 * Represents a valid document object of a document.
 */
export default interface DocumentDocument extends Document {
  name: string;
  modificationDate: number;
  lines: string[];
  author: UserDocument;
  contributors: UserDocument[];
}
