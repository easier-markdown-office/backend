import { Document } from 'mongoose';

/**
 * Represents a valid document object of an user.
 */
export default interface UserDocument extends Document {
  username: string;
  email: string;
  password: string;
  tokens: string[];
}
