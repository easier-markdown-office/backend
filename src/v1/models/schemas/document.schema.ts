import { model, Schema } from 'mongoose';
import { collectionNames } from '../../../../configs/config.json';
import DocumentDocument from '../documents/document.document';

const DocumentSchema = new Schema<DocumentDocument>(
  {
    name: {
      type: Schema.Types.String,
      required: true,
    },
    modificationDate: {
      type: Schema.Types.Number,
    },
    lines: [
      {
        type: Schema.Types.String,
      },
    ],
    author: {
      type: Schema.Types.ObjectId,
      ref: collectionNames.user,
    },
    contributors: [
      {
        type: Schema.Types.ObjectId,
        ref: collectionNames.user,
      },
    ],
  },
  { toObject: { virtuals: true }, toJSON: { virtuals: true } },
);

/**
 * The model for the document is set and exported for use within classes and functions.
 */
const DocumentDBM = model<DocumentDocument>(
  collectionNames.document,
  DocumentSchema,
);
export default DocumentDBM;
