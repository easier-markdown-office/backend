/**
 * Specifies the room prefixes of the possible websockets rooms.
 */
export enum ERoom {
  /** Indicates the room of the user (not client). This means all socket clients that have authenticated as this user. */
  user = 'user_',
  /** Indicates the room of the client (no prefix). */
  client = '',
  /** Indicates the room of the current document. */
  document = 'doc_',
  /** Indicates the room of documents which */
  collaborator = 'coll_',
  /** Indicates the room of the current document (no prefix). */
  currentDocument = 'sel_',
}
